# SQLab

SQLab is a simple utility for producing and grading SQL exercises with Moodle
interface in mind. It currently supports PostgreSQL and SQLite.

For detail instructions please consult the [Manual](docs/README.md)

If you have any questions and/or suggestions, please contact the author [Sławek Staworko](mailto:staworko@gmail.com)


## Credits
