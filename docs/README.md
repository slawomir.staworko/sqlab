# SQLab Manual

[[_TOC_]]

SQLab is simple tool for preparing, administering, and grading tests of (basic)
SQL using Moodle environment. 


## Installation 

In order to run SQLab your system needs to have installed:

* [Python 3](https://www.python.org)
* [Pandoc](https://pandoc.org)
* (optional) [PostgreSQL](https://www.postgresql.org)

Your Python3 installation needs to have the following modules installed:

* `pypandoc`
* `jinja2`
* `lxml`
* (optional) `psycopg2` (if you intend to use PostgreSQL databases)

Installing those modules is relatively straightforward with [PIP](https://pip.pypa.io/en/stable/installing/).

After installing the necessary modules, you need to download SQLab (either using git or by downloading and decompressing the packaged archive). 

The executable files are in the `bin` directory. Once all the dependencies are installed, you may add the `bin` directory to your `$PATH` variable.


## Usage 
Before the first use it is important to revise the [configuration file](#configuration-file), and in particular, to specify the SMTP connection parameters.

The typical SQLab work-flow is presented in the diagram below

![SQLab workflow](./img/flow.png)

### Preparing test

The test file contains a specification of the test. In essence, it is a pure SQL
file with a series of SQL code exercises interleaved with comments that contain
descriptions and parameter values. Below is an annotated example of a test file
([examples/test.sql](..examples/employees/test.sql)). 

![Example of test file](./img/example-test-file.png)

For ease of parsing, the test file is organized in relatively simple manner: it
consits of **SQL code blocks** (no comments) and **comment blocks** that contain
descriptions and parameter values. Recall that in SQL, a comment begins with
`--`. It is important to carefully use comments in a test file and, in
particular:

* _empty lines_ can be freely used in an code block but **not** in a comment
  block, where they will indicate the end of the comment block. 
* _empty comment lines_ can be be used in comment blocks, where they an be used
  for organization and starting new paragraphs, but **not** in a code block,
  where they will indicate the end of the code block.

The general strurecture of a test file is as follows:
    
1. **Header** which contains 
   * **Test name** that fits into a single line of test 
   * **Test description** that consists of multiple lines of text. 
   * **Test parameters** that are lines with `$key:value` pairs. 
2. **Mandatory empty line** that separates the header from exercises. 
3. **Exercises** that can be of one of the two kinds:
   1. **Simple query exercise** that consists of the exercise statement, its
      parameters, and a code block with a query that is the reference
      solution. 
      
      The first exercise from the test file above yields the following Moodle
      rendering

      ![Moodle rendering of exercise 1](img/example-exercise-1-moodle-rendering.png)
        
   2. **Data manipulation exercise** that additionally contains two additional
      parts: 
      * the optional **display** part, which allows for instance to add an
        snippet of table presenting the affected rows.
      * the **verification** part that allows to specify the query used to
        verify the correctness of the solution.
      
      This second exercise from the test file above yields the following Moodle
      rendering
      
      ![Moodle rendering of exercise 2](img/example-exercise-2-moodle-rendering.png)

      
      **IMPORTANT:** The additional parts need to be introduced with
      **mandatory** tags `#display_query` and `#verification_query`
      respectively.

The detailed list of recognized test file parameters can be found
[here](#test-file-parameters)

Once you have finished preparing the test file, you need to run SQLab to prepare
the Moodle exam. Assuming your test file name is `test.sql` the command is

``` bash
sqlab prepare test.sql
```

A successful execution will create an XML file `moodle.xml` with the
specification of exercises in a format that Moodle can understand. (The
execution will also create other files in the `prepared` directory that you may
find useful).


### Administering test on Moodle

The generated file `moodle.xml` can be used to import into Moodle the list of
exercises together with their descriptions and result snippets like the two
exercises above. The rendering is a result of a [jinja2
template](..templates/exercise-description.md) that can be modified to suit your
particular needs (see also [list of exercise template
variables](#exercise-template-variables)).

To administer the test on Moodle, you need to first upload it as illustrated in
the following video.

![Creating Moodle test](./mov/moodle-create-test.mp4)

Don't forget to configure the test as needed. (You can copy the title and test
description from the generated file `prepared/text.txt`).

Once the test is uploaded and administered, you need to download the the answers
in the form of a CSV file. **Make sure you have downloaded the correct CSV file
containing the answers** by following the instructions on the following video.

![Download answers from Moodle](./mov/moodle-download-answers.mp4)


### Grading answers

Grading answers with SQLab is straightforward with the following command
(assuming the answers are in the file `answers.csv`):

``` bash
sqlab grade answers.csv
```

SQL will grade the submitted answers and output the grades to
`answers-grades.csv`.


#### Grading databases

A number of different databases can be used for grading, each grading database
potentially used to test for a specific case. Grading databases can be specified
with the test parameter `$grading_dbs` in the test file header. By default the
main database specified with `$main_db` is also used for testing unless the test
parameter `$exclude_main_db_from_grading` is set to `false`. Consequently, if
only `$main_db` is specified it is the database used to both prepare snippets
for the Moodle test and to grade the submitted answers.

When multiple databases are used for grading, the potentially different results
grading the same exercise answer are combined with an aggregation function. By
default this function is `min` but it can be specified with the parameter
`$grading_aggregate`. Essentially, when multiple grading databases are used with
the function `min`, students answers need to return the same results as the
reference answer on all databases. 


#### Grading process 
An answer from the Moodle answer file `answers.csv` is evaluated by comparing
the results with those obtained with the reference query specified in the test
file.

* For a **simple query exercise**, it is the reference answer whose results are
  used for comparison. The student answer is evaluated in `read-only` mode i.e.,
  an attempt to modify the database state by the query will result in an error.
  
* For a **data manipulation exercise**, it is the verification query specified
  in the test file whose result is used to compare the student answer and the
  reference answer. The queries are evaluated in `rollback` mode i.e., after
  execution of an answer (reference or submitted) the state of the database is
  restored to the one before the execution.
  
SQLab attempts to detect the mode of an exercise automatically but it can be
changed manually with an exercise parameter `$mode`.


#### Grading metric
Evaluating an answer consists of comparing the results of student answer with
the results obtained with the reference answer. There are two kinds of issues
that the evaluation process can detect:

* **Capital offenses** which indicate a serious problem with the student answer
  and result in capital penalty of 100% (the score is 0):
  * `QUERY_EMPTY` the student submitted an empty query
  * `QUERY_TIMEOUT` the submitted query failed to execute within allotted time 
  * `QUERY_ERROR` execution of the submitted answer resulted in errors 
  * `ATTR_COUNT` the number of columns differs from those of the reference
    answer
  * `RESULT_SET` the set of tuples differs from the set of tuples of the
    reference answer
* **Punishable offenses** which indicate a possible problem with the student
  answer that may need to be penalized 
  * `ATTR_NAMES` the names of the columns differ from the name of the columns
    of the reference answer 
  * `RESULT_UNIQUE` there are duplicates in the results
  * `RESULT_ORDER` the order of the results differs from the order of results of
    the reference answer
    
To determine the score of an answer, a penalty metric is used. It specifies a
penalty for each punishable offense that is applied to the score, which
initially is equal to the exercise parameter `$points` (by default 1). A penalty
can be relative e.g., 50% meaning that half of the total points is subtracted,
or absolute e.g., 1 meaning that one point is subtracted. A _null_ penalty (0%
or 0) is used to indicate that a given offense is in fact not relevant. Score
cannot be negative: if the sum of penalties surpasses the points, the score of
the submitted answer is 0.

SQLab allows to define the main penalty metric for the whole test and overwrite
its elements in each exercise. A simple detection algorithm is used to guess
whether the order or the presence of duplicates are relevant. The penalty values
can be specified with test and exercise parameters:

  * `$penalty_for_wrong_attribute_names` allows to specify the penalty for wrong
    attribute names (by default it is 0% i.e., column names are not relevant). 
  * `$penalty_for_results_not_distinct` allows to specify the penalty for
    results that have repetitions (100% by default unless SQLab believe that
    uniqueness is not relevant)
  * `$penalty_for_wrong_order` allows to specify the penalty for results with
    wrong order (100% by default unless SQLab believe that order of the results
    is not relevant)
    

#### Example 

For the example test file above, the values of the main penalty metric are

``` json
"main_penalty_metric": {
    "ATTR_NAMES": "0%",
    "RESULT_ORDER": "100%",
    "RESULT_UNIQUE": "100%"
},
```

The penalty metric for the first exercise is 

``` json
"overlay_penalty_metric": {
    "ATTR_NAMES": "1",
    "RESULT_ORDER": "50%",
    "RESULT_UNIQUE": "0%"
},

```

and the penalty metric for the second exercise is

``` json
"overlay_penalty_metric": {
    "RESULT_ORDER": "0%"
    "RESULT_UNIQUE": "0%"
},
```

As an example suppose that a student, Jean Smith, submits the following answer
to the first exercise:

``` SQL
  SELECT City, AVG(Salary)
    FROM Employee JOIN Department ON Employee.Dept_ID = Department.ID
   WHERE Job = 'Secretary'
GROUP BY City
  HAVING AVG(Salary) > 13000
```

The systems uses the main database to evaluate this answer and determines that
the attribute names are not correct and that the order of the answers is not
correct. Consequently, the score of this answer is 3 - 1 - 50\%*3 = 0.5. 

Now, suppose that the student has submitted the following answer to the second
exercise:

``` SQL
UPDATE Employee
   SET Salary = 1.1 * Salary;
```

SQLab will evaluate both the submitted answer and afterwards the verification
query of the exercise `SELECT * FROM Employee;`. The result of the verification
query will be compared with the result of the verification query run after the
reference answer. NB: that the student answer and the reference answer are
evaluated in isolation using `rollback` mode and hence do not permanently affect
the state of the database. Because the submitted query is too _generous_ (it
gives a raise to all employees), the system identifies the offense `RESULT_SET`
and administers the capital penalty of 100%. Consequently, the score is 0.


### Sending feedback

Once the grading has been performed, SQLab allows to generate feedback using the
command `sqlab feedback`. This command has a several options that can be used to
control its behavior :

* `--include-answers` (or `-a`) indicates that reference answers should be included
  in the feedback
* `--include-grades` (or `-g`) indicates that final grade should be indicated in
  the feedback
* `--send-email` (or `-e`) indicates that the feedback should be sent by email
  to the test participants.
  
By default SQLab generates timestamped feedback files in the `feedback`
directory. For instance, if the following command is run on a file with the
answers of Jean Smith

``` bash
sqlab --include-answers --include-grades feedback answers.csv
```

the system will generate a feedback file with the following contents

``` txt
SUBJECT: Test results for General SQL evaluation.


Hello Smith Jean,

Below please find your results for General SQL evaluation.

You have successfully solved 0 out of 2 exercises.

Your grade is 0.8333333333333333/.

Detailed feedback follows.


EXERCISE 1 
===========
Find the average salary of a secretary for every city but leave only 
cities with average salary greater than 13000. Order the results by 
the average salary in decreasing order. Make sure the column names 
are precisely as on the output below.

The correct answer:
**********************************************************************
SELECT City, AVG(Salary) AS "Average Salary"
FROM Employee JOIN Department ON Employee.Dept_ID = Department.ID
WHERE Job = 'Secretary'
GROUP BY City
HAVING "Average Salary" > 13000
ORDER BY "Average Salary";
**********************************************************************
Your answer:
**********************************************************************

    SELECT City, AVG(Salary)
      FROM Employee JOIN Department ON Employee.Dept_ID = Department.ID
     WHERE Job = 'Secretary'
  GROUP BY City
    HAVING AVG(Salary) > 13000

**********************************************************************
Your result is 0.25 points out of 2.5 possible.
Issue: Wrong column names; Penalty: 1
Issue: Wrong order of results; Penalty: 50%


EXERCISE 2 
===========
Give a 10% raise to every employee who works in a department in Lille 
under the supervision of Helmut Ardelpic. 

The result of the following query will be used to verify your solution.
SELECT * FROM Employee;


The correct answer:
**********************************************************************
UPDATE Employee
SET Salary = 1.1 * Salary
WHERE Employee.ID IN (
SELECT Employee.ID
FROM Employee JOIN Department ON Employee.Dept_ID = Department.ID
JOIN Supervisor ON Supervisor.Employee_ID = Employee.ID
JOIN Employee AS S ON Supervisor.Supervisor_ID = S.ID
WHERE Department.City = 'Lille'
AND S.First_Name = 'Ardelpic' AND S.Last_Name = 'Helmut'
);
**********************************************************************
Your answer:
**********************************************************************
UPDATE Employee
     SET Salary = 1.1 * Salary;

**********************************************************************
Your result is 0.0 points out of 3.5 possible.
Issue: Wrong set of results; Penalty: 100%


Please reply to this email if you have any questions or comments. 
```

If you wish to send the generated feedback to the participants of the test, use
the appropriate option like this

``` bash
sqlab -ega feedback answers.csv
```


## Detailed information

### Command line arguments
SQLab command line recognizes the following arguments:

* `-h`, `--help` show a help message (and exits)
* `-c <config-file>`, `--config <config-file>` specify the location of the configuration file
* `-s <char>`, `--csv-sep <char>` specifies the separator for CSV files
  (default: `DETECT`). By default SQLab attempts to automatically detect the
  used separator. Also, in all output CSV files SQLab uses the comma `,` as the
  separator.
  

For the `feedback` command, additionally the following arguments are recognized:
* `-a`, `--include-answers` include reference answers in feedback 
* `-g`, `--include-grades`  include final grades in feedback 
* `-e`, `--send-emails`     send feedback by email



### Configuration file
The configuration file stores a number of default values used throughout
SQLab. The default name of the configuration file is `.sqlabrc`. SQLab attempts
to find the configuration file using the following search list:

1. A configuration file specified with the option `--config` (or `-c`)
2. `.sqlabrc` in the current directory
3. `.sqlabrc` in the home directory of the user
4. `.sqlabrc` in the subdirectory `config` in the application directory

The configuration file allows to define the following values:
1. Directories
    * `TEMPLATES_DIR` directory with templates (default: subdirectory `templates` in the application directory)
    * `TEMP_DIR` directory where temporary files can be places; must be writable (default: `/tmp`)
    * `PREPARED_DIR` name of the directory where prepared test files are to be
      placed; may be relative and does not need to exist or it will be created (default: `prepared`)
    * `FEEDBACK_DIR` name of the directory where prepared test files are to be
      placed; may be relative and does not need to exist or it will be created (default: `feedback`)
2. File names
   * `TEST_JSON` name of the json file storing parsed test file (default: `test.json`)
   * `MOODLE_XML` name of the file with Moodle list of exercises (default: `moodle.xml`)
   * `DESCRIPTION_TXT` name of the file with test description in pure text (default: `test.txt`)
   * `DESCRIPTION_TEX` name of the file with test description in LaTeX text (default: `test.tex`)
   * `DESCRIPTION_HTML` name of the file with test description in HTML5 (default: `test.html`)
   * `DESCRIPTION_MD` name of the file with test description in Markdown (default: `test.md`)
3. Query engine
   * `TIMEOUT` number of second that a query is allowed to run for before timeout (default: `10`)
4. Scoring (cf. [Grading metric](#grading-metric))
   * `points` number of points per exercise (default: `1`) 
   * `max_relative_grade`  maximum relative grade (default: `20`)
   * `penalty_for_wrong_attribute_names` (default: `0%`)
   * `penalty_for_results_not_distinct` (default: `100%`)
   * `penalty_for_wrong_order` (default: `100%`)
   * `grading_aggregate` (default: `min`)
5. Rendering result snippets
    * `show_rows` number of rows to display in result snippets; `all` to show all rows (default: `5`)
   * `MIN_COLUMN_WIDTH` minimum number of character of a column when rendering a table; must be at least `5` (default: `5`)
   * `MAX_COLUMN_WIDTH` minimum number of character of a column when rendering a table; must be at least `10` (default: `30`)
6. Templates: names of jinja templates 
   * `EXERCISE_DESCRIPTION` template file name for exercise description (for generating Moodle test)
   * `TEST_DESCRIPTION` template file name for test descritpion (for generating HTML and LaTeX files)
   * `HTML_STYLEUP_XSLT` template file name for styling result snippets for Moodle test
   * `FEEDBACK_EMAIL_CONTENT` template file name for feedback email content
   * `FEEDBACK_EMAIL_SUBJECT` template file name for feedback email subject (single line)
   * `ERROR_FEEDBACK` configuration file with one line explanations of query issues
7. SMTP server connection parameters
   * `SMTP_DEBUG` debug flag for the SMTP server
   * `SMTP_SERVER` address of an SMTP server for sending feedback
   * `SMTP_PORT` port of an SMTP server for sending feedback; use `0` to use the
     default port for the (SSL) SMTP protocol
   * `SMTP_LOGIN` login 
   * `SMTP_PASSWORD` password
   * `EMAIL_CC` address of carbon copy of every feedback email; may be empty
   * `EMAIL_FROM` address of sender 
   * `EMAIL_REPLY_TO` replay address for a feedback email; may be empty


### Test file parameters
Test can be configured with parameters specified in comment blocks.  The name of
parameter starts with the dollar sign `$`, is terminated by the colon `:`, and
followed by the parameter value (trailing spaces are removed from the parameter
value). A parameter must be used on a single comment line

```
-- $param_name: param_value
```


#### Test parameters
* `$main_db` coordinates of the main database, used for generating the query
  snippets in the Moodle exercise list. Currently, two type of databases are
  used: 
     * SQLite3 the coordinates consist of file path to a SQLite3 database file. 
     * PostgreSQL the coordinates consist of a [PostgreSQL connection URL](https://www.postgresql.org/docs/current/libpq-connect.html#LIBPQ-CONNSTRING)
       which is typically a string of the form `postgresql://user:pass@host:port/dbname` 
       (but can be as simple as `postgresql:///dbname`)  
  **IMPORTANT**: If main database is not specified, the system will attempt to
  use for this purpose the first file with extension `.db` or `.sqlite` that can
  be found in the current directory.
* `$grading_dbs` coordinates of additional databases used for grading purposes. 
* `$exclude_main_db_from_grading` setting this variable to `true` removes the
  main database from testing purposes provided that grading databases have been
  specified (default: `false`)
* `$max_relative_grade` the maximum grade a participant can obtains (default:
  `20`)
* `$grading_aggregate` the name of the aggregation function for combining the
  scores from different grading databases. Currently, only one aggregation
  function is supported `min`.
* `$penalty_for_wrong_attribute_names` the standard penalty for wrong attribute
  names if you wish to override the default value from the configuration file
  (default: `0%`)
* `$penalty_for_results_not_distinct` the standard penalty for results that have
  duplicates if you wish to override the default value from the configuration
  file (default: `100%`)
* `$penalty_for_wrong_order` the standard penalty for results that are in the
  wrong order if you wish to override the default value from the configuration
  file (default: `100%`)


#### Exercise parameters
* `$points` the number of points for a given exercise (default: `1`) 
* `$penalty_for_wrong_attribute_names` the penalty for wrong attribute names
* `$penalty_for_results_not_distinct` the penalty for results that have
  duplicates
* `$penalty_for_wrong_order` the standard penalty for results that are in the
  wrong order
* `$show_verification_query` set to `true` if you wish to display the
  verification query. (default: `false`)
* `$show_rows` the number of row of the answer to display or `all` if all rows
  are to be displayed (default: `5`)
* `$show_results_row_count` whether the Moodle exercise description should
  indicate the number of rows returned by the query (default: `true` for simple
  queries and `false` for data minipulation queries)
* `$show_update_row_count` whether the Moodle exercise description should
  indicate the number of lines affected by the reference answer to a data
  manipulation query (default: `false` for simple exercises and `true` for data
  manipulation exercises)
* `$mode` either `read_only` if the answer to the exercise does not need to
  modify the database; otherwise `rollback`. By default SQLab will attempt to detect
  the correct value. 
  

### Exercise template variables 
TBD
