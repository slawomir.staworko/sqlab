CREATE TABLE Department(
   ID     INTEGER PRIMARY KEY,
   Name   VARCHAR(255),
   City   VARCHAR(255)
);

CREATE TABLE Employee(
   ID            INTEGER NOT NULL PRIMARY KEY, 
   First_Name    VARCHAR(255),
   Last_Name     VARCHAR(255),
   Job           VARCHAR(255),
   Dept_ID       INTEGER NOT NULL REFERENCES DEPARTAMENT(ID),
   Salary        FLOAT
);

CREATE TABLE Supervisor(
   Employee_ID INTEGER PRIMARY KEY REFERENCES EMPLOYEE(ID),
   Supervisor_ID INTEGER REFERENCES EMPLOYEE(ID)
);

INSERT INTO Department VALUES(10,'Finance','Paris');
INSERT INTO Department VALUES(20,'Workshop','Grenoble');
INSERT INTO Department VALUES(30,'Workshop','Lille');
INSERT INTO Department VALUES(31,'Sales','Paris');
INSERT INTO Department VALUES(32,'Sales','Grenoble');
INSERT INTO Department VALUES(33,'Sales','Lille');
INSERT INTO Department VALUES(34,'Sales','Marseille');
INSERT INTO Department VALUES(35,'Sales','Lyon');
INSERT INTO Department VALUES(41,'Distribution','Paris');
INSERT INTO Department VALUES(42,'Distribution','Grenoble');
INSERT INTO Department VALUES(43,'Distribution','Lille');
INSERT INTO Department VALUES(44,'Distribution','Marseille');
INSERT INTO Department VALUES(45,'Distribution','Lyon');
INSERT INTO Department VALUES(50,'Administration','Paris');

INSERT INTO Employee VALUES(1,'Patamob','Adhemar','CEO',50,50000);
INSERT INTO Employee VALUES(2,'Zeublouze','Agathe','Distribution director',41,35000);
INSERT INTO Employee VALUES(3,'Kuzbidon','Alex','Sales Director',31,34000);
INSERT INTO Employee VALUES(4,'Locale','Anasthasie','Finance Director',10,36000);
INSERT INTO Employee VALUES(5,'Teutmaronne','Armand','Administration Director',50,36000);
INSERT INTO Employee VALUES(6,'Zoudanlkou','Debbie','Logistic Manager',41,25000);
INSERT INTO Employee VALUES(7,'Rivenbusse','Elsa','Logistic Manager',42,24000);
INSERT INTO Employee VALUES(8,'Ardelpic','Helmut','Logistic Manager',43,23000);
INSERT INTO Employee VALUES(9,'Peursconla','Humphrey','Logistic Manager',44,22000);
INSERT INTO Employee VALUES(10,'Vrante','Helena','Logistic Manager',45,21000);
INSERT INTO Employee VALUES(11,'Enfaillite','Melusine','Sale Representative',31,25000);
INSERT INTO Employee VALUES(12,'Eurktumeme','Odile','Sale Representative',32,26000);
INSERT INTO Employee VALUES(13,'Hotdeugou','Olaf','Sale Representative',33,27000);
INSERT INTO Employee VALUES(14,'Odlavieille','Pacome','Sale Representative',34,25500);
INSERT INTO Employee VALUES(15,'Amartakaldire','Quentin','Sale Representative',35,23000);
INSERT INTO Employee VALUES(16,'Traibien','Samira','Secretary',41,15000);
INSERT INTO Employee VALUES(17,'Fonfec','Sophie','Secretary',41,14000);
INSERT INTO Employee VALUES(18,'Fairant','Teddy','Secretary',42,13000);
INSERT INTO Employee VALUES(19,'Blaireur','Terry','Secretary',42,13000);
INSERT INTO Employee VALUES(20,'Ajerre','Tex','Secretary',43,13000);
INSERT INTO Employee VALUES(21,'Chmonfisse','Thierry','Secretary',43,12000);
INSERT INTO Employee VALUES(22,'Phototetedemort','Thomas','Secretary',44,22500);
INSERT INTO Employee VALUES(23,'Kaecoute','Xavier','Secretary',34,11500);
INSERT INTO Employee VALUES(24,'Adrouille-Toultan','Yves','Secretary',45,11000);
INSERT INTO Employee VALUES(25,'Anchier','Yvon','Secretary',45,10000);

INSERT INTO Supervisor VALUES (1,NULL);
INSERT INTO Supervisor VALUES (2,1);
INSERT INTO Supervisor VALUES (3,1);
INSERT INTO Supervisor VALUES (4,1);
INSERT INTO Supervisor VALUES (5,1);
INSERT INTO Supervisor VALUES (6,2);
INSERT INTO Supervisor VALUES (7,2);
INSERT INTO Supervisor VALUES (8,2);
INSERT INTO Supervisor VALUES (9,2);
INSERT INTO Supervisor VALUES (10,2);
INSERT INTO Supervisor VALUES (11,3);
INSERT INTO Supervisor VALUES (12,3);
INSERT INTO Supervisor VALUES (13,3);
INSERT INTO Supervisor VALUES (14,3);
INSERT INTO Supervisor VALUES (15,3);
INSERT INTO Supervisor VALUES (16,6);
INSERT INTO Supervisor VALUES (17,6);
INSERT INTO Supervisor VALUES (18,7);
INSERT INTO Supervisor VALUES (19,7);
INSERT INTO Supervisor VALUES (20,8);
INSERT INTO Supervisor VALUES (21,8);
INSERT INTO Supervisor VALUES (22,9);
INSERT INTO Supervisor VALUES (23,9);
INSERT INTO Supervisor VALUES (24,10);
INSERT INTO Supervisor VALUES (25,10);
