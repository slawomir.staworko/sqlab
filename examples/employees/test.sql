
-- 
-- General SQL evaluation
--
-- The purpose of this test is to evaluate your knowledge of SQL.  
-- You will work with a database consisting of two tables: 
-- 
--  * Department(ID,Name,City) 
--
--  * Employee(ID,First_Name,Last_Name,Job,Dept_ID,Salary) 
--
--  * Supervisor(Employee_ID,Supervisor_ID)
-- 
-- $main_db: employees.db
-- 

-- 
-- Find the average salary of a secretary for every city but leave only 
-- cities with average salary greater than 13000. Order the results by 
-- the average salary in decreasing order. Make sure the column names  
-- are precisely as on the output below.
--
-- $points: 2.5
-- $penalty_for_wrong_attribute_names: 1
-- $penalty_for_wrong_order: 50%
--
    SELECT City, AVG(Salary) AS "Average Salary"
      FROM Employee JOIN Department ON Employee.Dept_ID = Department.ID
     WHERE Job = 'Secretary'
  GROUP BY City
    HAVING "Average Salary" > 13000
  ORDER BY "Average Salary";

--
-- Give a 10% raise to every employee who works in a department in Lille 
-- under the supervision of Helmut Ardelpic.
--
-- $points: 3.5
--

  UPDATE Employee
     SET Salary = 1.1 * Salary
   WHERE Employee.ID IN (
     SELECT Employee.ID   
       FROM Employee JOIN Department ON Employee.Dept_ID = Department.ID
       JOIN Supervisor ON Supervisor.Employee_ID = Employee.ID
       JOIN Employee AS S ON Supervisor.Supervisor_ID = S.ID
      WHERE Department.City = 'Lille'
        AND S.First_Name = 'Ardelpic' AND S.Last_Name = 'Helmut'
    );

--
-- #display_query 
-- After this operation the information about the employees working in a
-- department in Lille should be as follows.
--
-- $show_results_row_count: false
  SELECT Employee.*
    FROM Employee JOIN Department ON Employee.Dept_ID = Department.ID
   WHERE Department.City = 'Lille'

-- #verification_query 
-- The result of the following query will be used to verify your solution.
-- $show_verification_query: true

  SELECT * FROM Employee;
