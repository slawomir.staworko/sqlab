## Exercise {{ exercise_no }}

{% if exercise_type == 'query' %}
{{ main_description }}

**Points**: {{ points }}

{% if show_results %}
{{ results }}

{% if show_results_row_count %}
*Line count*: {{ results_row_count }}
{% endif %}
{% endif %}


{% elif exercise_type == 'update' %}
{{ main_description }}

{% if show_update_row_count %}
**Affected row count**: {{ update_row_count }}
{% endif %}

**Points** {{ points }}

{% if display_description %}
{{ display_description }}
{% endif %}

{% if show_results %}
{{ results }}

{% if show_results_row_count %}
*Line count*: {{ results_row_count }}
{% endif %}
{% endif %}

{% if verification_description %}
{{ verification_description }}
{% endif %}

{% if show_verification_query %}
```
{{ verification_query }}
```
{% endif %}

{% endif %}
