<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method='html'/>

<xsl:template match="root">
    <xsl:apply-templates select="*|text()"/>
</xsl:template>

<xsl:template match="table">
   <xsl:copy>
      <xsl:apply-templates select="@*" /> 
      <xsl:attribute name="style">border: 1px solid #002b36; background-color: #002b36;</xsl:attribute>
      <xsl:apply-templates select="node()" /> 
   </xsl:copy> 
</xsl:template>



<xsl:template match="tr[@class='header']">
   <xsl:copy>
      <xsl:apply-templates select="@*" /> 
      <xsl:attribute name="style">text-align: center; background-color: #93a1a1;</xsl:attribute>
      <xsl:apply-templates select="node()" /> 
   </xsl:copy> 
</xsl:template>

<xsl:template match="tr[@class='odd']">
   <xsl:copy>
      <xsl:apply-templates select="@*" /> 
      <xsl:attribute name="style">text-align: right; background-color: #eee8d5;</xsl:attribute>
      <xsl:apply-templates select="node()" /> 
   </xsl:copy> 
</xsl:template>

<xsl:template match="tr[@class='even']">
   <xsl:copy>
      <xsl:apply-templates select="@*" /> 
      <xsl:attribute name="style">text-align: right; background-color: #fdf6e3;</xsl:attribute>
      <xsl:apply-templates select="node()" /> 
   </xsl:copy> 
</xsl:template>

<xsl:template match="td|th">
   <xsl:copy>
      <xsl:apply-templates select="@*" /> 
      <xsl:attribute name="style">border: 1px solid</xsl:attribute>
      <xsl:apply-templates select="node()" /> 
   </xsl:copy> 
</xsl:template>

<!--xsl:template match="tr[@class='odd']">
   <xsl:copy>
      <xsl:apply-templates select="@*" /> 
      <xsl:attribute name="style">border: 1px solid rgb(51, 51, 51);</xsl:attribute>
      <xsl:apply-templates select="node()" /> 
   </xsl:copy> 
</xsl:template>

<xsl:template match="tr[@class='even']">
   <xsl:copy>
      <xsl:apply-templates select="@*" /> 
      <xsl:attribute name="style">border: 1px solid rgb(51, 51, 51);</xsl:attribute>
      <xsl:apply-templates select="node()" /> 
   </xsl:copy> 
</xsl:template-->


<xsl:template match="node()|@*" > 
   <xsl:copy> 
      <xsl:apply-templates select="node()|@*" /> 
   </xsl:copy> 
</xsl:template> 

</xsl:stylesheet>
