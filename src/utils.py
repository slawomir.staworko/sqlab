import os
import sys
import csv
import pprint

import log



def get_app_dir():
    import pathlib 
    # return  pathlib.Path(os.path.dirname(sys.argv[0])).parent 
    return os.path.join(os.path.dirname(sys.argv[0]),'..')

def to_pretty_str(o):
    return pprint.pformat(o)

def remove_duplicates(l):
    res = []
    for e in l:
        if e not in res:
            res.append(e)
    return res

def str_to_bool(s):
    return str(s).lower() in ['yes','1','true','t']

def check_bool(d,key):
    if key not in d:
        return False
    return d.get(key,False)



# postgresql://[user[:password]@][netloc][:port][,...][/dbname][?param1=value1&...]
# postgresql:///musica
# postgresql://staworko:password@localhost:5432/musica
# database.db # sqlite
def parse_db_config(db_config_string):
    if db_config_string.startswith('postgresql://'):
        import psycopg2
        db_config = {
            'engine' : 'postgresql',
            'connection' : psycopg2.extensions.parse_dsn(db_config_string)
        }
    else : #sqlite file name
        db_config = {
            'engine' : 'sqlite',
            'connection' : db_config_string 
        }

    return db_config

def db_config_to_str(db_config):
    if db_config['engine'] == 'postgresql':
        #postgresql://someone@example.com/somedb?connect_timeout=10"
        result = 'postgresql://'
        if 'user' in db_config['connection']:
            result += db_config['connection']['user'] + '@'
        if 'host' in db_config['connection']:
            result += db_config['connection']['host']
        if 'host' in db_config['connection']:
            result += db_config['connection']['host']
        result += '/' + db_config['connection']['dbname']
        #return psycopg2.extensions.make_dsn(**db_config['connection'])
        return result
    else:
        return 'sqlite://'+db_config['connection']

def normalize_text(s):
    if not s:
        s = ''
    s = s.replace(' ',' ')
    s = s.replace('\t',' ')
    return s


def needs_rebuilding(goal_file, dependencies=None):
    if not os.path.exists(goal_file):
        return True
    if dependencies:
        for dep in dependencies:
            if os.path.exists(dep):
                if os.stat(goal_file).st_mtime < os.stat(dep).st_mtime:
                    log.info('%s fresher than %s'%(dep, goal_file))
                    return True

    return False

def find_csv_sep(csv_file, column_count, csv_sep=None):
    def check_csv_sep(csv_sep):
        with open(csv_file,'r') as f:
            reader = csv.reader(f,delimiter=csv_sep)
            header = next(reader)
            if len(header) == column_count:
                return True
            else:
                return False

    if csv_sep:
        if check_csv_sep(csv_sep):
            return csv_sep
        else:
            return None
    
    possible_csv_separators =[',', ';', ' ', '\t']
    for csv_sep in possible_csv_separators:
        if check_csv_sep(csv_sep):
            return csv_sep
    else:
        return None
        

if __name__ == '__main__':
    print(get_app_dir())
