import os
import shutil
import sqlite3

import log
import sql
import utils
import timeout



def normalise_tuple(row):
    t = []
    for cell in row:
        t.append(cell)
    return tuple(t)



#
# query_result : {
#     'query'   : string
#     'columns' : tuple(str)
#     'tuples'  : list(tuple)
#     'error'   : string
# }
#


class SessionManager:
    def __init__(self, env, db_config):
        self.env = env
        self.db_config = db_config
        self.active_session = None

    def create(self, mode):
        assert not self.active_session

        if self.db_config['engine'] == 'postgresql':
            session = PostgresSession(self.env, self.db_config, mode)
        elif self.db_config['engine'] == 'sqlite':
            session = SQLiteSession(self.env, self.db_config, mode)
        else:
            log.error("Unknown engine type %s",(self.db_config,))

        # timeout session
        
        if int(self.env.config['TIMEOUT']):
            self.active_session = timeout.setup_timeout(session,int(self.env.config['TIMEOUT']))
        else:
            self.active_session = session

        return self.active_session

    def release(self, session):
        assert self.active_session == session
        self.active_session.close()
        self.active_session = None


class SQLiteSession:
    def __init__(self, env, db_config, mode):
        assert mode in ['read_only','rollback']
        self.mode = mode 
        try:
            if self.mode == 'read_only':
                db_uri = 'file:'+db_config['connection']+'?mode=ro'
                self.db = sqlite3.connect(db_uri,uri=True)
            else: # mode == 'rollback'
                self.db_clone = os.path.join(env.config['TEMP_DIR'], 
                                             os.path.basename(db_config['connection'])+'.tmp')
                shutil.copyfile(db_config['connection'],self.db_clone)
                self.db = sqlite3.connect(self.db_clone)
            
        except FileNotFoundError as e:
            log.error('SQLite database file %s cannot be open.'%(db_config['connection'],))
        except sqlite3.OperationalError as e:
            log.error('Error attempting to open SQLite database %s\nReason:'%(utils.db_config_to_str(db_config),),e)

    def execute(self, query):
        assert self.db, 'Attempting to use a closed database session'
        res = {
            'query'     : query,
            'columns'   : None,
            'tuples'    : None,
            'error'     : None,
            'update_row_count' : None
        }

        cursor = self.db.cursor()

        try:
            cursor.execute(query)
        except Exception as e:
            res['error'] = '[' + type(e).__name__ + '] ' + str(e)
            return res

        if cursor.description:
            res['columns'] = tuple([str(d[0]) for d in cursor.description])
        res['tuples'] = []
        for row in cursor:
            res['tuples'].append(normalise_tuple(row))
        if sql.is_update(query):
            res['update_row_count'] = cursor.rowcount
        
        return res

    def close(self):
        self.db.rollback()
        self.db.close()
        self.db = None
        if self.mode == 'rollback':
            os.remove(self.db_clone)
        pass


# if __name__ == '__main__':
#     from test import env
    

    # session = SQLiteSession(env,{'connection':'../test/uni-test.db'},'rollback')
    # result = session.execute("CREATE VIEW StudentHobbies AS SELECT ID, Hobbies||'+' FROM Student;")
    # print(result)
    # result = session.execute('UPDATE Student SET Hobbies = Hobbies || \'+\';')
    # print(result)
    # result = session.execute('SELECT * FROM StudentHobbies;')
    # print(result)
    # session.close()
    # session = SQLiteSession(env,{'connection':'../test/uni-test.db'},'read_only')
    # result = session.execute('SELECT * FROM StudentHobbies;')
    # print(result)
    # session.close()

    

class PostgresSession:
    def __init__(self, env, db_config, mode):
        import psycopg2
        assert mode in ['read_only', 'rollback']
        self.mode = mode
        try:
            self.db = psycopg2.connect(**db_config['connection'])
            if mode == 'read_only':
                self.db.set_session(readonly=True)
        except psycopg2.OperationalError as e:
            log.error('Unable to connect to PostgreSQL database %s\nReason'%(utils.db_config_to_str(db_config)),e)

    def execute(self, query):
        res = {
            'query'     : query,
            'columns'   : None,
            'tuples'    : None,
            'error'     : None,
            'update_row_count' : None
        }

        if sql.is_transactional(query):
            log.warning("WARNING: Prohibited transaction control keyword used")
            res['error'] = "[TRANSACTION] Possible transactional tempering"
            return res

        cursor = self.db.cursor()

        try:
            cursor.execute(query)
        except Exception as e:
            res['error'] = '[' + type(e).__name__ + '] ' + str(e)
            return res
        if cursor.description:
            res['columns'] = tuple([str(d[0]) for d in cursor.description])
        res['tuples'] = []
        try:
            for row in cursor:
                res['tuples'].append(normalise_tuple(row))
        except psycopg2.ProgrammingError as e:
            assert str(e) == 'no results to fetch'
            pass

        if sql.is_update(query):
            res['update_row_count'] = cursor.rowcount
        
        return res

    def close(self):
        if self.mode == 'rollback':
            self.db.rollback()

# if __name__ == '__main__':
#     from test import env

#     db_config = {'connection': {'dbname': 'uni',
#                                 'host': 'localhost',
#                                 'port': '5432',
#                                 'user': 'staworko'}}

#     session = PostgresSession(env,db_config,'rollback')
    
#     result = session.execute("CREATE VIEW StudentHobbies AS SELECT ID, Hobbies||'+' FROM Student;")
#     print(result)
#     result = session.execute('UPDATE Student SET Hobbies = Hobbies || \'+\';')
#     print(result)
#     result = session.execute('SELECT * FROM StudentHobbies;')
#     print(result)
#     result = session.execute('COMMIT;')
#     print(result)
#     session.close()
#     session = PostgresSession(env,db_config,'rollback')
#     result = session.execute('SELECT * FROM StudentHobbies;')
#     print(result)
#     session.close()


class DatabaseEngine:
    def __init__(self, env, db_config):
        self.env = env
        self.db_config = db_config

        self.session_manager = SessionManager(self.env, self.db_config)
            
        self.reference_results = None

    def abort_exercise(self, exercise_no, results, stage='main_query'):
        log.error('Executing %s %d against %s\
                   \n=== QUERY ===\n%s\
                   \n\nLINE: %d of %s\
                   \n=== MESSAGE ===\n%s'%(
                       stage,
                       exercise_no, 
                       self.db_config['connection'],
                       self.env.test['exercises'][exercise_no][stage],
                       self.env.test['exercises'][exercise_no][stage+'_line_no'],
                       self.env.test['test_file'],
                       results['error']))

    def display_exercise_results(self, exercise_no):
        exercise = self.env.test['exercises'][exercise_no]
        session = self.session_manager.create(exercise['mode'])
        try:
            results = session.execute(exercise['main_query'])
            if results['error']:
                self.abort_exercise(exercise_no, results, 'main_query')
            if exercise['exercise_type'] == 'update':
                update_row_count = results['update_row_count']
            if exercise['display_query']:
                results = session.execute(exercise['display_query'])
                if results['error']:
                    self.abort_exercise(exercise_no, results, 'display_query')
            if exercise['exercise_type'] == 'update':
                results['update_row_count'] = update_row_count
            return results
        finally:
            self.session_manager.release(session)    

    def run_exercise(self, exercise_no, main_query=None, abort_on_error=False):
        exercise = self.env.test['exercises'][exercise_no]

        if main_query is not None:
            if not main_query.strip():
                return {
                    'query'     : main_query,
                    'columns'   : None,
                    'tuples'    : None,
                    'error'     : "Empty Query",
                    'update_row_count' : None
                }
            exercise = exercise.copy()
            exercise['main_query'] = main_query
        
        session = self.session_manager.create(exercise['mode'])
        try:
            results = session.execute(exercise['main_query'])
            if results['error']:
                if abort_on_error:
                    self.abort_exercise(exercise_no, results, 'main_query')
                return results
            if exercise['verification_query']:
                results = session.execute(exercise['verification_query'])
                if results['error']:
                    if abort_on_error:
                        self.abort_exercise(exercise_no, results, 'verification_query')
            return results
        finally:
            self.session_manager.release(session)    


    def compute_reference_results(self):
        if self.reference_results is not None:
            return 
        log.info("Precomputing reference results for", utils.db_config_to_str(self.db_config))
        self.reference_results = {}
        for exercise_no in self.env.test['exercises']:
            main_query = self.env.test['exercises'][exercise_no]['main_query']
            results = self.run_exercise(exercise_no, main_query, True)
            if len(results['tuples']) == 0:
                log.warning('WARNING: Empty reference results for exercise',exercise_no,
                            'cf. line', self.env.test['exercises'][exercise_no]['main_query_line_no'],
                            'of', self.env.test['test_file'])
                
            #print(results)
            self.reference_results[exercise_no] = results

def create_main_engine(env):
    return DatabaseEngine(env, env.test['main_db'])

def create_grading_engine(env, pos):
    return DatabaseEngine(env, env.test['grading_dbs'][pos])
                          
if __name__ == '__main__':
    import os

    import env


    with open('../test/engine-test.sql','w') as f:
        f.write("""
        -- Test name.
        -- $main_db: postgresql:///unia
        -- $grading_dbs: ../test/uni.db ../test/uni.db

        -- Info text 1.1
        -- Info text 1.2
        -- 
        -- Info text 1.3
        -- $mode:rollback
        SELECT * FROM Student;
        -- Info 2
        SELECT DISTINCT First_Name 
          FROM Student;
        -- Info 3
        SELECT First_Name FROM Student ORDER BY Height;
        -- Update query
        UPDATE Student SET Height = Height /100.0;
        """)
        f.close()

    env = env.create_env('debug ../test/engine-test.sql',['test_file'])

    print(env)

    engine = env.get_main_engine()
    for db_no in range(env.get_grading_engine_count()):
        engine = env.get_grading_engine(db_no)

    if os.path.exists('test.json'):
        os.remove('test.json')


