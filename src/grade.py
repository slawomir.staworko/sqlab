import csv 
import json

import log
import utils
import assess



class GradingMachine:

    def __init__(self, env):
        self.env = env
        self.grades = None         

    def grade_exercise(self, engine_no, exercise_no, answer):
        #log.debug('ANSWER',answer,bool(answer))
        results = self.env.get_grading_engine(engine_no).run_exercise(exercise_no, answer)
        #log.debug('RESULT',results)
        reference_results = self.env.get_grading_engine(engine_no).reference_results[exercise_no]
        issues = assess.assess_exercise_results(reference_results, results)
        metric = assess.compose_metrics(self.env.test['main_penalty_metric'],
                                        self.env.test['exercises'][exercise_no]['overlay_penalty_metric'])
        grading = assess.elaborate_grade(self.env.test['exercises'][exercise_no]['points'],
                                             issues,metric) 
        grading['error'] = results['error']
        return grading

    def warmup_grading_engines(self):
        for i in range(self.env.get_grading_engine_count()):
            self.env.get_grading_engine(i)

    def load_answers(self):
        self.answers, self.names = self.env.get_answers()

# grades : {
#    email : {
#       'name'       : str,
#       'grade'      : float,
#       'correct'    : int,
#       'answers'    : {
#          exercise_no : {
#             'answer'     : str,
#             'score'      : float,
#             'correct'    : bool,
#             'grading'    : {
#                engine_no : {
#                   'assessment' : <grade-elaboration>,
#                   'points' : float,
#                   'error' : str
#                }
#             }
#          }
#       }
#    }
# }
    def grade(self):
        self.warmup_grading_engines()
        log.info("Grading answers in '%s'"%(self.env.get_answers_file_name(),))
        input_answers, names = self.env.get_answers()

        self.grades = {}
        aggr = self.env.get_grading_aggregate()
        for email in names:
            log.info('  Grading %-20s'%(names[email],), no_new_line=True)
            self.grades[email] = {}
            self.grades[email]['name'] = names[email]
            self.grades[email]['correct'] = 0
            answers = {}
            for ex_no in self.env.test['exercises']:
                answers[ex_no] = {}
                answers[ex_no]['answer'] = input_answers[email][ex_no]
                answers[ex_no]['correct'] = True
                answers[ex_no]['grading'] = {}
                points_list = []
                for db_no in range(self.env.get_grading_engine_count()):
                    grading = self.grade_exercise(db_no, ex_no, answers[ex_no]['answer'])
                    #log.debug('  Ex', ex_no, grading)
                    answers[ex_no]['grading'][db_no] = grading
                    if grading['assessment']:
                        answers[ex_no]['correct'] = False
                    points_list.append(grading['points'])
                answers[ex_no]['score'] = aggr(points_list)
                if answers[ex_no]['correct']:
                    self.grades[email]['correct'] += 1
                    
            self.grades[email]['answers'] = answers
            grade = self.calculate_final_grade(email)
            log.info(' %d/%d %.1f'%(self.grades[email]['correct'], len(self.env.test['exercises']), grade))
        self.calculate_final_grades()

    def calculate_final_grade(self, email):
        max_points = sum([self.env.test['exercises'][ex_no]['points']
                          for ex_no in self.env.test['exercises']])

        scores_list = [self.grades[email]['answers'][ex_no]['score'] 
                       for ex_no in self.env.test['exercises']]
        grade = sum(scores_list)/max_points*self.env.test['max_relative_grade']
        
        return grade


    def calculate_final_grades(self):
        assert self.grades

        for email in self.grades:
            grade = self.calculate_final_grade(email)

            if 'grade' in self.grades[email] and self.grades[email]['grade'] != grade:
                log.info('Changed scores of %s give new final grade %.1f instead of %.1f'%(
                    self.grades[email]['name'], grade, self.grades[email]['grade'])
                )

            self.grades[email]['grade'] = grade

# in general the grades CSV file has this header 
# ...-grades.csv: email, name, grade, correct, (answer ex#ex_no, score ex#ex_no, (points ex#ex_no db#db_no, assess ex#ex_no db#db_no, error ex#ex_no db#db_no)*)*
# but if only one test db is given then we have a simpler header
# ...-grades.csv: email, name, grade, correct, (answer #ex_no, score #ex_no, assess #ex_no, error #ex_no)*
    def save_grades(self):
        log.info("Saving grades to '%s'"%(self.env.get_grades_file_name()))

        header = ['email','name', 'grade', 'correct']
        for ex_no in self.env.test['exercises']:
            if self.env.get_grading_engine_count() == 1:
                header.extend(['answer #'+str(ex_no),'score #'+str(ex_no),'issues #'+str(ex_no),'error #'+str(ex_no)])
            else:
                header.extend(['answer ex#'+str(ex_no),'score ex#'+str(ex_no)])
                for db_no in range(self.env.get_grading_engine_count()):
                    header.extend(['points ex#'+str(ex_no)+' db#'+str(db_no),
                                   'issues ex#'+str(ex_no)+' db#'+str(db_no),
                                   'error ex#'+str(ex_no)+' db#'+str(db_no)])

        # answer = self.grades[email]['answers'][ex_no]
        def add_answer(row, ex_no, answer):
            if self.env.get_grading_engine_count() == 1:
                row['answer #'+str(ex_no)] = answer['answer']
                row['score #'+str(ex_no)] = answer['score']
            else:
                row['answer ex#'+str(ex_no)] = answer['answer']
                row['score ex#'+str(ex_no)] = answer['score']
        # grading = self.grades[email]['answers'][ex_no]['grading'][db_no]
        def add_grading(row, ex_no, db_no, grading):
            #print('adding grading',grading)
            if self.env.get_grading_engine_count() == 1:
                row['issues #'+str(ex_no)] = json.dumps(grading['assessment']) if grading['assessment'] else None
                row['error #' +str(ex_no)] = grading['error']
            else:
                row['points ex#'+str(ex_no)+' db#'+str(db_no)] = grading['points']
                row['issues ex#'+str(ex_no)+' db#'+str(db_no)] = json.dumps(grading['assessment']) if grading['assessment'] else None
                row['error ex#'+str(ex_no)+' db#'+str(db_no)] = grading['error']

        description_row = {k:None for k in header}
        reference_row = {k:None for k in header}
        description_row['email'] = ' DESCRIPTION'
        reference_row['email'] = ' REFERENCE ANSWERS'
        reference_row['grade'] = self.env.test['max_relative_grade']
        for ex_no in self.env.test['exercises']:
            add_answer(description_row, ex_no, {'answer': self.env.test['exercises'][ex_no]['main_description'],
                                                'score' : self.env.test['exercises'][ex_no]['points']})
            add_answer(reference_row, ex_no, {'answer': self.env.test['exercises'][ex_no]['main_query'], 
                                              'score' : None})
            for db_no in range(self.env.get_grading_engine_count()):
                add_grading(description_row, ex_no, db_no, {'points':self.env.test['exercises'][ex_no]['points'], 'assessment':None, 'error':None})
                add_grading(reference_row, ex_no, db_no, {'points':None, 'assessment':None, 'error':None})

        
        with open(self.env.get_grades_file_name(),'w') as f:
            writer = csv.DictWriter(f,header)
            writer.writeheader()
            writer.writerow(description_row)
            writer.writerow(reference_row)
            for email in self.grades:
                row = {
                    'email'  : email, 
                    'name'   : self.grades[email]['name'], 
                    'grade'  : self.grades[email]['grade'],
                    'correct': self.grades[email]['correct']
                }
                answers = self.grades[email]['answers']
                for ex_no in self.grades[email]['answers']:
                    answer = answers[ex_no]
                    add_answer(row, ex_no, answer)
                    for db_no in range(self.env.get_grading_engine_count()):
                        grading = answer['grading'][db_no]
                        add_grading(row, ex_no, db_no, grading)
                writer.writerow(row)


            f.close()

    def load_grades(self):
        log.info("Loading grades from '%s'"%(self.env.get_grades_file_name()))
        def answer_col(ex_no):
            if self.env.get_grading_engine_count() == 1:
                return 'answer #' + str(ex_no)
            else:
                return 'answer ex#' + str(ex_no)
        def score_col(ex_no):
            if self.env.get_grading_engine_count() == 1:
                return 'score #' + str(ex_no)
            else:
                return 'score ex#' + str(ex_no)
        def points_col(ex_no, db_no):
            if self.env.get_grading_engine_count() == 1:
                return 'score #' + str(ex_no)
            else:
                return 'points ex#' + str(ex_no) + ' db#' + str(db_no)
        def issues_col(ex_no, db_no):
            if self.env.get_grading_engine_count() == 1:
                return 'issues #' + str(ex_no)
            else:
                return 'issues ex#' + str(ex_no) + ' db#' + str(db_no)
        def error_col(ex_no, db_no):
            if self.env.get_grading_engine_count() == 1:
                return 'error #' + str(ex_no)
            else:
                return 'error ex#' + str(ex_no) + ' db#' + str(db_no)

        grades_file_name = self.env.get_grades_file_name()
        if self.env.get_grading_engine_count() == 1:
            column_count = 4 + len(self.env.test['exercises'])*4
        else:
            column_count = 4 + len(self.env.test['exercises'])*(2+3*self.env.get_grading_engine_count())
        csv_sep = utils.find_csv_sep(grades_file_name, column_count, self.env.params['csv_sep'])
        if not csv_sep:
            log.error('Unable to parse the CSV file',grades_file_name,
                      'using the specified CSV separator\n' if self.env.params['csv_sep'] else '\n',
                      'Make sure you have downloaded the right file')
            

        reader = csv.DictReader(open(grades_file_name),delimiter=csv_sep)
        next(reader) # discard DESCRIPTION row
        next(reader) # discard REFERENCE ANSWER row
        # row = next(reader)
        grades = {}
        for row in reader: 
            email = row['email']
            grades[email] = {
                'name' : row['name'], 
                'grade' : float(row['grade']),
                'correct' : int(row['correct'])
            }
            answers = {}
            for ex_no in self.env.test['exercises']:
                answers[ex_no] = {
                    'answer' : row[answer_col(ex_no)],
                    'score' : float(row[score_col(ex_no)]),
                    'correct' : True,
                    'grading' : {}
                }
                for db_no in range(self.env.get_grading_engine_count()):
                    answers[ex_no]['grading'][db_no] = {
                        'assessment' : json.loads(row[issues_col(ex_no,db_no)]) if row[issues_col(ex_no,db_no)] else [],
                        'points' : float(row[points_col(ex_no,db_no)]),
                        'error' : row[error_col(ex_no,db_no)] if row[error_col(ex_no,db_no)] else None
                    }
                    if answers[ex_no]['grading'][db_no]['assessment']:
                        answers[ex_no]['correct'] = False
            grades[email]['answers'] = answers

        self.grades = grades
        self.calculate_final_grades()
        
        return grades

if __name__ == '__main__':
    import env, os

    if os.path.exists('test.json'):
        os.remove('test.json')

    env = env.create_env('prepare ../test/cc/test.sql ../test/cc/answers-small.csv',['test_file','answers_file'])

    print(env)

    ge = GradingMachine(env)
    ge.grade()
    print(utils.to_pretty_str(ge.grades))
    print('saving...')
    ge.save_grades()
    print('loading')
    ge.load_grades()
    print(utils.to_pretty_str(ge.grades))
