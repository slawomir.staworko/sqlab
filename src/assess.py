import log

#
# Possible query issues
#
QUERY_EMPTY    = "QUERY_EMPTY"    # Query is empty
QUERY_TIMEOUT  = "QUERY_TIMEOUT"  # Query failed to execute within reasonable time 
QUERY_ERROR    = "QUERY_ERROR"    # Query execution errors 
ATTR_COUNT     = "ATTR_COUNT"     # Wrong number of columns
ATTR_NAMES     = "ATTR_NAMES"     # Wrong column names
RESULT_SET     = "RESULT_SET"     # Results sets disagree  
RESULT_UNIQUE  = "RESULT_UNIQUE"  # Duplicates present in results
RESULT_ORDER   = "RESULT_ORDER"   # Wrong order of results
SOLUTION_OK    = "SOLUTION_OK"    # Solution has passed the test

# Query assessment assessment flow
#
#         QUERY_EMPTY (capital penalty 100%)
#              |
#              |
#             \|/
#         QUERY_TIMEOUT (capital)
#              |
#              |
#             \|/
#         QUERY_ERROR (capital)
#              |
#              |
#             \|/
#         ATTR_COUNT (capital)
#           /   \
#          |     |
#         \|/   \|/
#  RESULT_SET   ATTR_NAMES (punishable)
# (capital) \   /   
#            \ /
#            \|/
#        RESULT_UNIQUE (punishable)
#             |
#             |
#            \|/
#        RESULT_ORDER  (punishable) 
#             |
#             |
#            \|/
#        SOLUTION_OK

def assess_exercise_results(reference_results, results):
    # capital offenses 
    if not results['query'].strip():
        return [QUERY_EMPTY]
    if results['error']:
        if results['error'] == '[TimeoutException]':
            return [QUERY_TIMEOUT]
        else:
            return [QUERY_ERROR]
    if len(reference_results['columns']) != len(results['columns']):
        return [ATTR_COUNT]
    if set(reference_results['tuples']) != set(results['tuples']):
        return [RESULT_SET]

    # punishable offenses
    issues = []
    if reference_results['columns'] != results['columns']:
        issues.append(ATTR_NAMES)
    if len(set(results['tuples'])) != len(results['tuples']):
        issues.append(RESULT_UNIQUE)
    if reference_results['tuples'] != results['tuples']:
        issues.append(RESULT_ORDER)

    return issues

#
# Capital offenses cause all points to be lost
#
CAPITAL_OFFENSES = [QUERY_EMPTY,QUERY_TIMEOUT,QUERY_ERROR,ATTR_COUNT,RESULT_SET]

#
# Punishable offenses cause some points to be lost
#
PUNISHABLE_OFFENSES = [ATTR_NAMES,RESULT_UNIQUE,RESULT_ORDER]

# 
# Penalties specification for punishable query issues 
# penalty_metric : {
#    ATTR_NAMES     : <penalty>
#    RESULT_UNIQUE  : <penalty>
#    RESULT_ORDER   : <penalty>
# }
#
# <penalty> is always a string, either a number of points to subtract or the
# percentage of total points to subtract 

def get_metric_raw_value(metric, issue):
    if metric[issue].endswith('%'):
        return float(metric[issue][:-1])
    else:
        return float(metric[issue])

def punish(points, full_points, issue, metric):
    if metric[issue].endswith('%'):
        points -= full_points*get_metric_raw_value(metric,issue)/100.0
    else:
        points -= get_metric_raw_value(metric,issue)

    if points < 0:
        return 0
    
    return points

def is_punishable(issue, metric):
    return metric[issue] and get_metric_raw_value(metric,issue)

def elaborate_grade(full_points, issues, metric):

    # capital offenses 
    for issue in CAPITAL_OFFENSES:
        if issue in issues:
            return {'points': 0, 
                    'assessment' : [{'issue':issue,'penalty':'100%'}]}

    # punishable offenses
    assessment = []
    points = full_points
    for issue in PUNISHABLE_OFFENSES:
        if is_punishable(issue,metric) and issue in issues:
            assessment.append({'issue':issue,'penalty':metric[issue]})
            points = punish(points, full_points, issue, metric)

    return {'points': points, 
            'assessment' : assessment}

def compose_metrics(main_metric, overlay_metric):
    metric = {}

    for k in main_metric:
        metric[k] = main_metric[k]
        if k in overlay_metric:
            metric[k] = overlay_metric[k]

    return metric


if __name__ == '__main__':

    penalty_metric = {
        ATTR_NAMES     : '25%',
        RESULT_UNIQUE  : '0%',
        RESULT_ORDER   : '3'
    }

    print(elaborate_grade(10,{SOLUTION_OK},penalty_metric))
    print(elaborate_grade(10,{QUERY_ERROR},penalty_metric))
    print(elaborate_grade(10,{ATTR_COUNT},penalty_metric))
    print(elaborate_grade(10,{ATTR_NAMES},penalty_metric))
    print(elaborate_grade(10,{RESULT_UNIQUE},penalty_metric))
    print(elaborate_grade(10,{ATTR_NAMES,RESULT_ORDER},penalty_metric))
    print(elaborate_grade(10,{ATTR_NAMES,RESULT_UNIQUE},penalty_metric))
