import os
import glob

import args
import log
import utils
import test
import config
import engine 
import prepare
import grade
import feedback

#
# The execution environment consists of 3 components:
# * config -- system wide configuration e.g., localization of templates 
# * test   -- session wide application information e.g., test specification and database location 
# * params -- execution wide paramters e.g., type of action and its parameters
#

def create_env(arguments, param_list):
    env = Environment()
    log.env = env
    if type(arguments) is str:
        arguments = args.parse_args(arguments)
    env.load_params(arguments, param_list)
    env.load_config()
    env.load_test()


    return env

class Environment:
    def __init__(self):
        self.config = None
        self.test = None
        self.params = None
        self.main_engine = None
        self.grading_engines = None

    def load_params(self, arguments, param_list):
        if self.params:
            log.error("Attempting to reread command line arguments") 

        self.params = dict()

        if arguments.csv_sep == 'DETECT':
            self.params['csv_sep'] = None
        elif type(arguments.csv_sep) == list and arguments.csv_sep:
            self.params['csv_sep'] = arguments.csv_sep[0]
        else:
            log.warn("WARNING Unrecognized CSV separator", arguments.csv_sep)
            self.params['csv_sep'] = None

        self.params['config']  = arguments.config
        self.params['send_emails']     = arguments.send_emails
        self.params['include_answers'] = arguments.include_answers
        self.params['include_grades']  = arguments.include_grades

        param_index = 0
        arg_index = 0
        while param_index < len(param_list):
            param_name = param_list[param_index]
            if param_name.endswith('*'):
                # load all remaining arguments into a list 
                if param_index != len(param_list) - 1:
                    log.error("A single *-param is allowed on the last position only")
                param_name = param_name[:-1]
                if arg_index < len(arguments.arg):
                    self.params[param_name] = [arguments.arg[i] for i in range(arg_index,len(arguments.arg))]
                else:
                    self.params[param_name] = None
                break
            elif arg_index < len(arguments.arg):
            
                self.params[param_name] = arguments.arg[arg_index]
                param_index += 1
                arg_index += 1
            else:
                #log.warning("No default value for parameter param %s"%(param_name,))
                self.params[param_name] = None            
                param_index += 1

    def load_config(self):
        assert self.params
        self.config = config.load_config(self.params)

    def guess_test_file(self):
        candidates = glob.glob('*.sql')
        if len(candidates) > 1:
            log.warning('Multiple test file candidates:', ' '.join(candidates))
            
        if not (candidates):
            return None

        log.info('Selecting test file:', candidates[0])
        return candidates[0]

    def load_test(self):
        test_file = None

        if 'test_file' in self.params:
            test_file = self.params['test_file']
        elif os.path.exists(self.config['TEST_JSON']):
            _test = test.load_test(self.config['TEST_JSON'])
            test_file = _test['test_file']
        else:
            test_file = self.guess_test_file()

        if not test_file:
            log.error('Unable to locate test file')

        if utils.needs_rebuilding(self.config['TEST_JSON'],[test_file]):
            self.test = test.build_test(self, test_file)
            test.save_test(self.test,self.config['TEST_JSON'])
        else:
            self.test = test.load_test(self.config['TEST_JSON'])

    def get_grading_aggregate(self):
        if self.test['grading_aggregate'] == 'min':
            return min
        else:
            log.error('Unknown aggregate',self.test['grading_aggregate'])


    #
    # Database Engines 
    #
    def get_main_engine(self):
        if not self.main_engine:
            self.main_engine = engine.create_main_engine(self)
            self.main_engine.compute_reference_results()
        return self.main_engine

    def get_grading_engine_count(self):
        return len(self.test['grading_dbs'])

    def get_grading_engine(self, db_no=0):
        if not self.grading_engines:
            self.grading_engines = [None] * len(self.test['grading_dbs'])
        if not self.grading_engines[db_no]:
            self.grading_engines[db_no] = engine.create_grading_engine(self,db_no)
            self.grading_engines[db_no].compute_reference_results()
        return self.grading_engines[db_no]

    #
    # Prepare
    #
    def prepare_test(self):
        prepare.prepare_test(self)

    #
    # Answers
    #
    def get_answers_file_name(self):
        if 'answers_file' in self.params:
            if not os.path.exists(self.params['answers_file']):
                log.error('Answer file', self.params['answers_file'],'cannot be open')
            return self.params['answers_file']
        else:
            log.error('Answer file not specified')

    def get_answers(self):
        import answers
        return answers.load_answers(self)

    #
    # Grades
    # 
    def get_grades_file_name(self):
        if 'grades_file' in self.params:
            return self.params['grades_file']
        elif 'answers_file' in self.params:
            base,ext = os.path.splitext(self.params['answers_file'])
            return base+'-grades'+ext
        else:
            log.error('Unable to find answer file')

    def grade_answers(self):
        gm = grade.GradingMachine(self)
        gm.grade()
        gm.save_grades()

    # def get_grades(self):
    #     gm = grade.GradingMachine(self)
    #     if utils.needs_rebuilding(self.get_grades_file_name(), [self.get_answers_file_name()]):
    #         gm.grade()
    #         gm.save_grades()
    #     else:
    #         gm.load_grades()
    #         # gm.save_grades()

    #     return gm.grades

    # 
    # Feedback
    # 
    def send_feedback(self):
        gm = grade.GradingMachine(self)
        grades = gm.load_grades()
        feedback.send_feedback(self,grades)

    def __str__(self):
        return '''
ENVIRONMENT
PARAMS
%s

CONFIG
%s 

TEST
%s
'''%(utils.to_pretty_str(self.params),
     utils.to_pretty_str(self.config),
     utils.to_pretty_str(self.test))


if __name__ == '__main__':

    with open('../test/env-test.sql','w') as f:
        f.write("""
        -- Test name.
        --
        -- This is 
        -- test description
        --
        -- It comprises of 
        -- many paragraphs
        -- $main_db: ../test/uni.db
        -- $grading_dbs: test1.db test2.db
        -- $penalty_for_wrong_attribute_names: 25%

        -- Info text 1.1
        -- Info text 1.2
        -- 
        -- Info text 1.3
        -- $penalty_for_wrong_attribute_names: 15%
        -- $points: 2.5
        SELECT * FROM Student;
        -- Info 2.1
        --
        -- Info 
        UPDATE Student SET Height = Height/100.0;

        -- #verification_query

        SELECT ID, Height FROM Student;

        -- #display_query

        SELECT ID, First_Name, Height FROM Student;

        
        -- Info 3
        DELETE FROM Student WHERE Parent_ID IS NULL;

        -- #verification_query
        -- What is that

        SELECT * FROM Student;
        
        """)
        f.close()

    env = create_env('prepare ../test/env-test.sql',['test_file'])
    print(env)
    try:
        env.get_main_engine().compute_reference_results()
    except Exception as e:
        log.error('UNhandled exception',exception=e)
    #os.remove('test.json')
    
