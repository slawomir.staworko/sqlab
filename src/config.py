import os
import string

import log
import check
import utils

def find_config(params):
    if params['config']:
        return params['config']
    elif os.path.exists('.sqlabrc'):
        return '.sqlabrc'
    elif os.path.exists(os.path.join(os.environ['HOME'],'.sqlabrc')):
        return os.path.join(os.environ['HOME'],'.sqlabrc')
    else:
        return os.path.join(utils.get_app_dir(),'config/.sqlabrc')

def load_config_file(config_file, config=None):

    if not config:
        config = {}
    with open(config_file,'r') as f:
        for line_no, line in enumerate(f.read().split('\n')):
            line = line.strip()
            if not line or line.startswith('#'):
                log.debug('Skipping line', line_no)
                continue
            pair = line.split('=')
            if len(pair) != 2:
                log.warning('Malformed line no %d in %s "%s"'%(line_no, config_file,line))
                continue
            key = pair[0].strip()
            value = pair[1].strip()
            if '$' in value:
                value = string.Template(value).substitute(config)
            config[key] = value
    return config


def load_config(params):
    config_file = find_config(params)
    # log.info("Loading configuration file",config_file)

    # interpolation = configparser.ExtendedInterpolation()
    # config = configparser.ConfigParser(dict_type=dict,
    #                                    interpolation=interpolation)

    # config.read_dict({'DIRS':{'app_dir':utils.get_app_dir()}})

    if not os.path.exists(config_file):
        log.error('Configuration file %s not found.'%config_file)

    config = load_config_file(config_file, {'APP_DIR':utils.get_app_dir()})

    check_config_integrity(config)

    return config


config_keys = [
    # 'DIRS'
    'APP_DIR',
    'TEMPLATES_DIR', 
    'TEMP_DIR', 
    'PREPARED_DIR', 
    'FEEDBACK_DIR',
    # 'FILES'
    'TEST_JSON',
    'MOODLE_XML',
    'DESCRIPTION_TXT',
    'DESCRIPTION_TEX',
    'DESCRIPTION_HTML',
    'DESCRIPTION_MD',
    # 'QUERY_ENGINE'
    'TIMEOUT',
    # 'SCORING'
    'points',
    'max_relative_grade', 
    'penalty_for_wrong_attribute_names', 
    'penalty_for_results_not_distinct',
    'penalty_for_wrong_order',
    'grading_aggregate',
    # 'RENDERING'
    'show_rows',
    'MIN_COLUMN_WIDTH',
    'MAX_COLUMN_WIDTH',
    # 'TEMPLATES'
    'EXERCISE_DESCRIPTION',
    'TEST_DESCRIPTION',
    'HTML_STYLEUP_XSLT',
    'FEEDBACK_EMAIL_CONTENT',
    'FEEDBACK_EMAIL_SUBJECT',
    'ERROR_FEEDBACK',
    # 'EMAIL'
    'SMTP_DEBUG',
    'SMTP_SERVER',
    'SMTP_PORT',
    'SMTP_LOGIN',
    'SMTP_PASSWORD',
    'EMAIL_CC',
    'EMAIL_FROM',
    'EMAIL_REPLY_TO',
]

def check_config_integrity(config):

    for key in config:
        if key not in config_keys:
            log.error('Unrecognized key %s in configuration key'%(key,))

    for key in config_keys:
        if key not in config:
            log.error('Configuration file is missing the value for key %s'%(key,))

    if not check.is_natural(config['TIMEOUT']):
        log.error('Configuration file parameter TIMEOUT must be a positive integer!')
    if not check.is_writable_dir(config['TEMP_DIR']):
        log.error('Configuration file parameter TEMP_DIR must be a writable directory!')
    if not check.is_dir(config['TEMPLATES_DIR']):
        log.error('Configuration file parameter TEMPLATES_DIR must be a readable directory!')
    if not check.is_int(config['MIN_COLUMN_WIDTH']):
        log.error('Configuration file parameter MIN_COLUMN_WIDTH must be an integer!')
    if int(config['MIN_COLUMN_WIDTH']) < 5:
        log.error('Configuration file parameter MIN_COLUMN_WIDTH must be at least 5!')
    if not check.is_int(config['MAX_COLUMN_WIDTH']):
        log.error('Configuration file parameter MAX_COLUMN_WIDTH must be an integer!')
    if int(config['MAX_COLUMN_WIDTH']) < 10:
        log.error('Configuration file parameter MAS_COLUMN_WIDTH must be at least 10!')

    for template_param in ['EXERCISE_DESCRIPTION','TEST_DESCRIPTION','HTML_STYLEUP_XSLT',
                           'FEEDBACK_EMAIL_CONTENT','FEEDBACK_EMAIL_SUBJECT','ERROR_FEEDBACK']:

        if not check.is_readable_file(config[template_param]):
            log.error('Configuration file parameter %s must be a readable template file!'%(template_param,))

    if not check.is_int(config['SMTP_PORT']) or int(config['SMTP_PORT']) < 0 or int(config['SMTP_PORT']) > 65535:
        log.error('SMTP port needs to be a valid port number: an integer in the range [0;65535]')

def convert(config):
    return { s: {k: config[s][k] for k in config[s]} for s in config}


if __name__ == '__main__':
    config = load_config({'config':None})
    for key in config:
        print(key,config[key])

    # config = load_config_file('../config/.sqlabrc-simple',{'APP_DIR':utils.get_app_dir()})
    # print(utils.to_pretty_str(config))
