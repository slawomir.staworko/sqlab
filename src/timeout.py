import signal


class TimeoutException(Exception):
    def __init__(self):
        pass

class TimeoutableSessionWrapper:
    def __init__(self, session, timeout):
        self.session = session
        self.timeout = timeout
    def execute(self, query):
        try:
            activate(self.timeout)
            res = self.session.execute(query)
        except TimeoutException as e:
            res = {
                'query'   : query,
                'columns' : None,
                'tuples'  : None,
                'error'   : '[' + type(e).__name__ + '] ' + str(e)
            }
        finally:
            deactivate()
        return res

    def close(self):
        self.session.close()

def setup_timeout(session, timeout):
    return TimeoutableSessionWrapper(session, timeout)

def timeout_handler(signum, frame):
    if signum == signal.SIGALRM:
        raise TimeoutException()
    
def activate(timeout):
    signal.signal(signal.SIGALRM, timeout_handler)
    signal.alarm(timeout)

def deactivate():
    signal.alarm(0)


if __name__ == '__main__':
    import os
    import args

    with open('../test/timeout-queries.sql','w') as f:
        f.write("""
        -- Timeout test.
        -- $main_db: ../test/music.db

        -- Fast Query
        SELECT * FROM Disc D1;
        -- Long query
        SELECT * FROM Disc D1, Disc D2, Disc D3, Disc D4;
        """)
        f.close()


    parser = args.make_parser()
    args = parser.parse_args('prepare ../test/timeout-queries.sql'.split())

    import env
    env = env.Environment()
    env.load_params(args,['test_file'])
    env.load_config()
    env.load_test()

    print(env)

    engine = env.get_main_engine()
