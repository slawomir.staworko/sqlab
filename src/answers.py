import csv

import log
import utils

# converts a string date info into an easy to compare tuple
# e.g., "24 septembre 2019  11:44" -> (2019,9,24,11,44)
def extract_timestamp(date_str):
    if not date_str:
        return None
    p = date_str.replace(':',' ').split(' ')
    if len(p) < 5:
        return None
    months = {'janvier': 1, 'février': 2, 'mars': 3, 'avril': 4, 'mai': 5, 'juin': 6,
              'juillet': 7, 'aout': 8, 'septembre': 9, 'octobre': 10, 'novembre': 11, 'décembre': 12}
    return (int(p[2]),months[p[1]],int(p[0]),int(p[4]),int(p[5]))

if __name__ == "__main__":
    assert extract_timestamp("24 septembre 2019  11:44") == (2019,9,24,11,44)


# loads from a given answer file (answers.csv exported from Moodle) into a
# simple dictionary answers : email -> query_no -> query
def load_answers(env):
    
    log.info("Loading answers from '%s' "%(env.get_answers_file_name(),))
    answer_file_name = env.get_answers_file_name()
    column_count = 8 + len(env.test['exercises'])
    csv_sep = utils.find_csv_sep(answer_file_name, column_count, env.params['csv_sep'])
    if not csv_sep:
        log.error('Unable to parse the CSV file',answer_file_name,
                  'using the specified CSV separator\n' if env.params['csv_sep'] else '\n',
                  'Make sure you have downloaded the right file')


    LAST_NAME = 0
    FIRST_NAME = 1
    EMAIL_ADDRESS = 2
    COMPLETED_ON = 5
    ANSWER = 7
    
    answers = dict()
    names = dict()
    timestamp = dict()
    try:
        reader = csv.reader(open(answer_file_name,'r'),
                            delimiter=csv_sep)
        next(reader)
        for row in reader:
            if not row:
                break
            name = row[LAST_NAME] + ' ' + row[FIRST_NAME]
            email = row[EMAIL_ADDRESS]
            if email in answers:
                t = extract_timestamp(row[COMPLETED_ON]) 
                if not t or timestamp[email] and t < timestamp[email]:
                    log.info('Skipping double entry', email)
                    continue
                log.warning('Fresher answer found for', email)
            solutions = dict()
            for exercise_no in env.test['exercises']:
                solution = row[ANSWER + exercise_no]
                if solution == '-':
                    solution = ''

                solutions[exercise_no] = utils.normalize_text(solution)
            timestamp[email] = extract_timestamp(row[COMPLETED_ON])

            answers[email] = solutions
            names[email] = name
    except FileNotFoundError as e:
        log.error('Unable to find the answer file', answer_file_name)
    except IndexError as e:
        log.error('Malformed answer file', answer_file_name,'\nDetails:',e)
    except ValueError as e:
        log.error('Malformed answer file', answer_file_name,'\nPossible cause: the 5th column is not a well-formed date\nDetails:',e)
        
    return answers, names

if __name__ == '__main__':
    import os
    import env
    if os.path.exists('test.json'):
        os.remove('test.json')
    env = env.create_env('debug ../test/cc/test.sql ../test/cc/answers.csv',
                         ['test_file','answers_file'])

    answers, names = load_answers(env)
    
    print(utils.to_pretty_str(answers))
    print(utils.to_pretty_str(names))
    if os.path.exists('test.json'):
        os.remove('test.json')

