import jinja2
import os.path

import log
import utils

    
class TemplateRunner:
    def __init__(self, env):
        self.env = env
        self.templates = {}
    def get_template(self, template_name):
        if template_name not in self.templates:
            try:
                self.templates[template_name] = jinja2.Template(open(self.env.config[template_name],'r').read())
            except jinja2.exceptions.TemplateSyntaxError as e:
                log.error('Error in template %s\nReason:'%(self.env.config[template_name],), e)
        return self.templates[template_name]

    def render(self, template_name, params):
        template = self.get_template(template_name)
        result = template.render(params)
        return result



if __name__ == '__main__':
    with open('../test/template-test.sql','w') as f:
        f.write("""
        -- Rendering test.
        -- Blah blah
        -- 
        -- Blah Blah
        -- $main_db:../test/uni.db

        -- Fast Query
        -- 
        -- And another paragraph.
        -- $points: 10
        SELECT * FROM Student WHERE First_Name IN ('Paul', 'Jean');

        -- Fast Query
        -- 
        -- And another paragraph.
        -- $points: 10
        UPDATE Student SET Height = Height/100.0

        -- #verification_query
        -- Bam, bam
        SELECT ID,First_Name,Height FROM Student;
        """)
        f.close()

    import env
    env = env.create_env('prepare ../test/template-test.sql',['test_file'])

    if os.path.exists('test.json'):
        os.remove('test.json')

