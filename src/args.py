import sys
import log
import argparse

usage="sqlab prepare <test_file>\n\
       sqlab grade <answers_file>\n\
       sqlab [-eag] feedback <answers_file>"


action_help="""Action to perform, can be one of the following:

* prepare  prepares test files, and in particular 
           Moodle test exercise list grade

* grade    grades the answers in the answers file

* feedback generate feedback for the answers 

"""



def make_parser():
    parser = argparse.ArgumentParser(prog='sqlab',
                                     usage=usage,
                                     formatter_class = argparse.RawTextHelpFormatter,
                                     description = 'Query test processing tool'
    )

    parser.add_argument('action', 
                        choices=['prepare', 'grade', 'feedback', 'debug'],
                        metavar="action",
                        help=action_help)
    parser.add_argument('arg', 
                        nargs="*",
                        help="Optional argument to be passed to actions")
    parser.add_argument('-c','--config',
                        nargs='?',
                        metavar='<config-file>',
                        help='configuration file')
    parser.add_argument('-s','--csv-sep', 
                        nargs=1, 
                        metavar='<char>',
                        default='DETECT',
                        help="separator for input CSV files")
    parser.add_argument('-a','--include-answers', 
                        action='store_true',
                        default=False,
                        help="include reference answers in feedback ")
    parser.add_argument('-g','--include-grades', 
                        action='store_true',
                        default=False,
                        help="include final grades in feedback ")
    parser.add_argument('-e','--send-emails', 
                        action='store_true',
                        default=False,
                        help="send feedback by email")

    return parser

def parse_args(s=None):
    parser = make_parser()
    if s:
        return parser.parse_args(s.split())
    else:
        return parser.parse_args()

if __name__ == '__main__':
    print(parse_args('prepare queries.sql'))
    print(parse_args('-a feedback'))
    print(parse_args('help'))
