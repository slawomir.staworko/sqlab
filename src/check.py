import os

def is_dir(p):
    return os.path.exists(p) and os.path.isdir(p)

def is_file(p):
    return os.path.exists(p) and os.path.isfile(p)

def is_writable_dir(p):
    return is_dir(p) and os.access(p,os.W_OK)

def is_readable_file(p):
    return is_file(p) and os.access(p,os.R_OK)

def is_exercise_mode(s):
    return s in ['read_only','rollback']

def is_penalty(s):
    if type(s) is not str:
        return False
    if s.endswith('%'):
        s = s[:-1]
    return is_float(s)


def is_float(s):
    s = str(s)
    try:
        float(s)
    except:
        return False
    return True

def is_int(s):
    s = str(s)
    try:
        int(s)
    except:
        return False
    return True

def is_natural(s):
    s = str(s)
    try:
        return int(s) >= 0
    except:
        return False
    return True

if __name__ == '__main__':
    assert is_float('1.2')
    assert is_float('1')
    assert is_int('0010')
    assert not is_int('1.2')
    assert not is_int('asd')
    assert is_penalty('1')
    assert is_penalty('10%')
    assert not is_penalty('%')
    assert not is_penalty('6tg')

