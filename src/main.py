import sys
import log
import os.path

import args

import env
if __name__ == '__main__':
    try:

        args = args.parse_args()

        if args.action == 'prepare':
            env = env.create_env(args, ['test_file']) 
            env.prepare_test()
        elif args.action == 'grade':
            env = env.create_env(args, ['answers_file']) 
            env.grade_answers()
        elif args.action == 'feedback':
            env = env.create_env(args, ['grades_file']) 
            env.send_feedback()
        else:
            pass
        log.repeat_warnings()
    except Exception as e:
        log.error('Exception', repr(type(e)), e)
