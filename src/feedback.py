import os
import os.path
import smtplib # import SMTP_SSL as SMTP
import email #.message # import EmailMessage
import datetime
import socket

import log
import utils
import config
import template


class Emailer:
    def __init__(self,env):
        self.env = env
        self.smtp_debug = env.config['SMTP_DEBUG']
        self.smtp_server = env.config['SMTP_SERVER']
        self.smtp_port = int(env.config['SMTP_PORT'])
        self.smtp_login = env.config['SMTP_LOGIN']
        self.smtp_password = env.config['SMTP_PASSWORD']
        self.email_cc = env.config.get('EMAIL_CC',None)
        self.email_from = env.config['EMAIL_FROM']
        self.email_reply_to = env.config['EMAIL_REPLY_TO']

        self.smtp_connection = None

    def shutdown(self):
        if self.smtp_connection:
            self.smtp_connection.quit()

    def establish_smtp_connection(self):
        if not self.smtp_server:
            log.error('No SMTP server specified in the configuration file')
        log.info('Establishing SMTP Connection to', self.smtp_server, '[10 seconds timeout]')
        try:
            self.smtp_connection = smtplib.SMTP_SSL(self.smtp_server,self.smtp_port,None,None,None,10)
        except socket.timeout as e:
            log.error('Unable to establish SMTP connection to %s: Attempt timed out'%(self.smtp_server,))
        except socket.gaierror as e:
            log.error('Unable to establish SMTP connection to %s: Invalid host name'%(self.smtp_server,))
        #print(self.smtp_debug,utils.str_to_bool(self.smtp_debug))
        self.smtp_connection.set_debuglevel(utils.str_to_bool(self.smtp_debug))
        try:
            self.smtp_connection.login(self.smtp_login,self.smtp_password)
        except TypeError as e:
            log.error('Unable to login to SMTP server. Possible causes include authentication error. Set SMTP_DEBUG to true for more details')

    def _send_email_message(self, msg):
        if not self.smtp_connection:
            self.establish_smtp_connection()
        
        self.smtp_connection.send_message(msg)

    def send_email(self,email_to, email_subject, email_content):
        msg = email.message.EmailMessage()
        msg['From'] = self.email_from
        msg['To'] = email_to
        if self.email_cc:
            msg['CC'] = self.email_cc
        if self.email_reply_to:
            msg['Reply-To'] = self.email_reply_to
        msg['Subject'] = email_subject
        msg.set_content(email_content)
        self._send_email_message(msg)

class FeedbackSender:
    def __init__(self, env, grades):
        self.env = env
        self.grades = grades 
        self.emailer = Emailer(env)
        self.template_runner = template.TemplateRunner(env)
        self.error_feedback = config.load_config_file(self.env.config['ERROR_FEEDBACK'])
        
    def load_error_feedback(self):

        self.error_feedback = {}
        with open(error_feedback_file,'r') as f:
            for line in f.read().split('\n'):
                line = line.strip()
                if not line or line.startswith('#'):
                    continue
                pair = line.split('=')
                if len(pair) != 2:
                    log.warning('Malformed line in %s... skipping'%(error_feedback_file,))
                key = pair[0].strip()
                value = pair[1].strip()
                self.error_feedback[key] = value
        return self.error_feedback

    def send_feedback(self):
        log.info('Generating feedback')
        for email in self.grades:
            params = self.grades[email].copy()
            params['params'] = self.env.params
            params['config'] = self.env.config
            params['test'] = self.env.test
            params['error_feedback'] = self.error_feedback
            email_content = self.template_runner.render('FEEDBACK_EMAIL_CONTENT',params)
            email_subject = self.template_runner.render('FEEDBACK_EMAIL_SUBJECT',params)

            if self.env.params['send_emails']:
                self.emailer.send_email(email, email_subject, email_content) 
                log.info('EMAIL SENT', email)
                

            now = datetime.datetime.now()
            with open(os.path.join(self.env.config['FEEDBACK_DIR'],
                                   email+'-at-'+now.strftime('%d-%m-%Y-%H-%M-%S.txt')),'w') as f:
                if self.env.params['send_emails']:
                    f.write('SENT\n\n')
                else:
                    f.write('UNSENT\n\n')
                f.write('SUBJECT: '+email_subject+'\n\n\n')
                f.write(email_content)
                f.close()

def send_feedback(env, grades):
    if not os.path.exists(env.config['FEEDBACK_DIR']):
        os.mkdir(env.config['FEEDBACK_DIR'])

    f = FeedbackSender(env,grades)
    f.send_feedback()
    

if __name__ == '__main__':
    import env, os, grade
    env = env.create_env('-ag debug ../test/cc/test.sql ../test/cc/answers-small.csv',['test_file','answers_file'])
    send_feedback(env,env.get_grades())
    # emailer = Emailer(env)
    # emailer.send_email('staworko@gmail.com','test','test')


