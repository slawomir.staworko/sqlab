import sys
import os
import os.path
import datetime

FINE = 0
DEBUG = 1
INFO = 2
IMPORTANT = 3
WARNING = 4
ERROR = 5

debug_level = INFO

warnings = []

env = None

def log(level, *msgs, **params):
    message = ''
    #message += '[' + datetime.datetime.now().strftime('%H:%M:%S') + ']: '
    message += ' '.join(map(str,msgs))
    if debug_level <= level:
        sys.stdout.write(message)
        if not params.get('no_new_line',False):
            sys.stdout.write('\n')
        sys.stdout.flush()

def debug(*msgs, **params):
    log(DEBUG, *msgs, **params)

def info(*msgs, **params):
    log(INFO, *msgs, **params)

def message(*msgs, **params):
    log(IMPORTANT, *msgs, **params)

def warning(*msgs, **params):
    warnings.append(' '.join(map(str,msgs)))
    log(WARNING, *msgs, **params)

def error(*msgs, **params):
    log(ERROR, *(tuple(['ERROR:']+list(msgs))), **params)
    prepare_error_report()
    sys.exit(-1)

def force(*msgs, **params):
    log(ERROR+1, *msgs, **params)

def progress(*msgs):
    message = ' '.join(map(str,msgs)) + '         \r'
    sys.stdout.write(message)
    sys.stdout.flush()

def repeat_warnings():
    for w in warnings:
        log(WARNING,'REPEATED',w)

def prepare_error_report():
    import traceback
    import datetime
    
    #traceback.print_exc()

    log_name = datetime.datetime.now().strftime("error-%d.%m.%Y-%H-%M-%S.log")
    with open(log_name,'w') as f:
        f.write(str(env))
        traceback.print_exc(file=f)
        f.close()

