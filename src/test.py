import sys
import json
import glob
import os
import os.path

import log
import sql
import utils
import assess
import engine
import parser

#
# test : {
#    'test_file'             : string,
#
#    'test_name'             : string,
#    'main_db'               : <db-config>,
#    'grading_dbs'           : [<db-config>,...]
# 
#    'max_relative_grade'    : float,
#    'grading_aggregate'     : string,
#    'main_penalty_metric'   : <metric>,
#    'exercises'                 : {
#       i : {
#          'exercise_no'                    : int
#          'exercise_type'                  : {'query', 'update'}
#     
#          'main_description'               : string,
#          'main_query'                     : string, 
#          'main_query_line_no'             : int,
#
#          'verification_description'       : string,
#          'verification_query'             : string, 
#          'verification_query_line_no'     : int,
#          'show_verification_query'        : bool,
#
#          'display_description'            : string, 
#          'display_query'                  : string, 
#          'display_query_line_no'          : int,
#          'show_display_query'             : bool,
#          
#          'show_rows'                      : int + 'all',
#          'show_results'                   : bool,
#          'show_results_row_count'         : bool,
#          'show_update_row_count '         : bool,
#
#          'points'                 : float,
#          'overlay_penalty_metric' : <partial-metric>
#          'mode'                   : ['read_only','rollback']
#       }
#    }
# }

def find_sqlite_candidates():
    candidates = glob.glob('*.db') + glob.glob('*.sqlite')
    candidates.sort()
    return candidates
    
def guess_main_db():
    sqlite_candidates = find_sqlite_candidates()
    if not sqlite_candidates:
        log.error('No main db configuation given and none can be guessed')
    if len(sqlite_candidates) > 1:
        log.info('Choosing the first SQLite databse file',
                 sqlite_candidates[0])
    return sqlite_candidates[0]


def build_test(env, test_file):
    log.info("Parsing test data in '%s'"%(test_file,))
    raw_test = parser.parse(test_file)
    log.debug(utils.to_pretty_str(raw_test))
    raw_test_params = raw_test['params']
    
    test = {'test_file' : test_file}
    
    test['test_name'] = raw_test['test_name']
    test['test_description'] = raw_test['test_description']

    if 'main_db' in raw_test_params:
        main_db = raw_test_params['main_db']
    else:
        main_db = guess_main_db()

    grading_dbs = []
    if 'grading_dbs' in raw_test_params:
        grading_dbs.extend(raw_test_params['grading_dbs'].split())
    if not utils.check_bool(raw_test_params,'exclude_main_db_from_grading') or not grading_dbs :
        grading_dbs.insert(0,main_db)
    grading_dbs = utils.remove_duplicates(grading_dbs)
    test['main_db'] = utils.parse_db_config(main_db)
    test['grading_dbs'] = list(map(utils.parse_db_config,grading_dbs))
    
    test['max_relative_grade'] = float(raw_test_params.get('max_relative_grade', 
                                                           env.config['max_relative_grade']))
    test['grading_aggregate'] = raw_test_params.get('grading_aggregate', 
                                                    env.config['grading_aggregate'])

    metric = {}
    metric[assess.ATTR_NAMES] = raw_test_params.get('penalty_for_wrong_attribute_names', 
                                                    env.config['penalty_for_wrong_attribute_names'])
    metric[assess.RESULT_UNIQUE] = raw_test_params.get('penalty_for_results_not_distinct', 
                                                       env.config['penalty_for_results_not_distinct'])
    metric[assess.RESULT_ORDER] = raw_test_params.get('penalty_for_wrong_order', 
                                                      env.config['penalty_for_wrong_order'])
    test['main_penalty_metric'] = metric

    test['exercises'] = {}
    for exercise_no in raw_test['exercises']:
        raw_test_exercise = raw_test['exercises'][exercise_no]
        exercise_params = raw_test_exercise['params']

        test_exercise = {'exercise_no' : exercise_no}

        test_exercise['main_description'] = raw_test_exercise['main_description']
        test_exercise['main_query'] = raw_test_exercise['main_query']
        test_exercise['main_query_line_no'] = raw_test_exercise['main_query_line_no']

        if 'verification_query' in raw_test_exercise['posts']:
            raw_verification_post = raw_test_exercise['posts']['verification_query']
            test_exercise['verification_description'] = raw_verification_post['description']
            test_exercise['verification_query'] = raw_verification_post['query']
            test_exercise['verification_query_line_no'] = raw_verification_post['query_line_no']
        else:
            test_exercise['verification_description'] = None
            test_exercise['verification_query'] = None
            test_exercise['verification_query_line_no'] = None
        test_exercise['show_verification_query'] = utils.str_to_bool(exercise_params.get('show_verification_query',False))

        if not test_exercise['verification_query']:
            test_exercise['exercise_type'] = 'query'
        else:
            test_exercise['exercise_type'] = 'update'

        if 'display_query' in raw_test_exercise['posts']:
            raw_display_post = raw_test_exercise['posts']['display_query']
            test_exercise['display_description'] = raw_display_post['description']
            test_exercise['display_query'] = raw_display_post['query']
            test_exercise['display_query_line_no'] = raw_display_post['query_line_no']
        else:
            test_exercise['display_description'] = None
            test_exercise['display_query'] = None
            test_exercise['display_query_line_no'] = None
        test_exercise['show_display_query'] = utils.str_to_bool(exercise_params.get('show_display_query',False))

        show_rows = exercise_params.get('show_rows',env.config['show_rows'])
        if show_rows == 'all':
            test_exercise['show_rows'] = 'all'
        else: 
            test_exercise['show_rows'] = int(show_rows)

        if test_exercise['exercise_type'] == 'query' or test_exercise['display_query']:
            test_exercise['show_results'] = utils.str_to_bool(exercise_params.get('show_results',True))
            test_exercise['show_results_row_count'] = utils.str_to_bool(exercise_params.get('show_results_row_count',True))
        else:
            test_exercise['show_results'] = utils.str_to_bool(exercise_params.get('show_results',False))
            test_exercise['show_results_row_count'] = utils.str_to_bool(exercise_params.get('show_results_row_count',False))
        if test_exercise['exercise_type'] == 'update':
            test_exercise['show_update_row_count'] = utils.str_to_bool(exercise_params.get('show_update_row_count',True))

        test_exercise['points'] = float(exercise_params.get('points',env.config['points']))
        test_query = test_exercise['main_query'] if not test_exercise['verification_query'] else test_exercise['verification_query']
        test_metric = sql.guess_penalty_metric(test_query)
        
        if 'penalty_for_wrong_attribute_names' in exercise_params:
            test_metric[assess.ATTR_NAMES] = exercise_params['penalty_for_wrong_attribute_names']
        if 'penalty_for_results_not_distinct' in exercise_params:
            test_metric[assess.RESULT_UNIQUE] = exercise_params['penalty_for_results_not_distinct']
        if 'penalty_for_wrong_order' in exercise_params:
            test_metric[assess.RESULT_ORDER] = exercise_params['penalty_for_wrong_order']
        test_exercise['overlay_penalty_metric'] = test_metric

        test_exercise['mode'] = exercise_params.get('mode', sql.guess_query_mode(test_exercise['main_query']))
        
        test['exercises'][exercise_no] = test_exercise
    return test


def save_test(test,test_json):
    log.info("Saving test data to '%s'"%(test_json,))
    test_json_str = json.dumps(test, indent=4, sort_keys=True,ensure_ascii=False)
    open(test_json,'w').write(test_json_str)

def load_test(test_json):
    log.info("Loading test data from '%s'"%(test_json,))
    test = json.load(open(test_json,'r'))
    exercises = { int(exercise_no): test['exercises'][exercise_no] 
              for exercise_no in test['exercises']}
    test['exercises'] = exercises
    return test

if __name__ == '__main__':
    import os
    import args

    args = args.parse_args('prepare ../test/test-test.sql')
    
    from env import Environment
    env = Environment()
    env.load_params(args,['test-file'])
    env.load_config()
    with open('../test/test-test.txt','w') as f:
        f.write("""
        -- Test name.
        --
        -- This is 
        -- test description
        --
        -- It comprises of 
        -- many paragraphs
        -- $main_db: ../test/uni.db
        -- $grading_dbs: test1.db test2.db
        -- $penalty_for_wrong_attribute_names: 25%

        -- Info text 1.1
        -- Info text 1.2
        -- 
        -- Info text 1.3
        -- $penalty_for_wrong_attribute_names: 15%
        -- $points: 2.5
        SELECT * FROM R;
        -- Info 2.1
        --
        -- Info 
        UPDATE
        R SET A = 0;
        
        -- #verification_query
        SELECT DISTINCT A FROM R;
        
        -- Info 3
        DELETE FROM R WHERE A IS NULL;

        -- #verification_query
        -- What is that
        SELECT * FROM R;

        -- #display_query
        -- $show_display_query: true
        SELECT * FROM R;
        
        """)
        f.close()
    
    test = build_test(env,'../test/test-test.txt')
    print(utils.to_pretty_str(test))
