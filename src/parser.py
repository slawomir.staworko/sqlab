import sys
import json
import glob
import os
import os.path

import log
import sql
import utils 

def tokenize(line_no,line):
    token = {
        'is_comment' : None,
        'is_empty'   : False,
        'is_text'    : False,
        'is_param'   : False,
        'is_tag'     : False,
        'text'       : None,
        'key'        : None,
        'value'      : None,
        'tag'        : None,
        'line'       : line,
        'line_no'    : line_no,
    }

    line = line.strip().strip('\n')

    if sql.is_comment(line):
        token['is_comment'] = True
        text = sql.remove_comment_prefix(line).strip()
        if not text:
            token['is_empty'] = True
        elif text.startswith('$') and ':' in text:
            key = text[1:text.index(':')]
            value = text[text.index(':')+1:]
            token['is_param'] = True
            token['key'] = key
            token['value'] = value
        elif text.startswith('#'):
            tag = text[1:]
            token['is_tag'] = True
            token['tag'] = tag
        else:
            token['is_text'] = True
            token['text'] = text
    else:
        text = line
        if not text:
            token['is_empty'] = True
        else:
            token['is_text'] = True
            token['text'] = text

    return token


if __name__ == "__main__":
    assert tokenize(0,'--')['is_comment']
    assert tokenize(0,'--')['is_empty']
    assert tokenize(0,'-- $hello:there')['is_param']
    assert tokenize(0,'-- $hello:there')['key'] == 'hello'
    assert tokenize(0,'-- $hello:there')['value'] == 'there'
    assert tokenize(0,'-- $boum:')['key'] == 'boum'
    assert tokenize(0,'-- $boum:')['value'] == ''



TEST_BEGIN                       = 'TEST_BEGIN'
TEST_NAME                        = 'TEST_NAME'
TEST_DESCRIPTION                 = 'TEST_DESCRIPTION'
EXERCISE_DESCRIPTION_BEGIN       = 'EXERCISE_DESCRIPTION_BEGIN'
MAIN_DESCRIPTION_BEGIN           = 'MAIN_DESCRIPTION'
MAIN_DESCRIPTION                 = 'MAIN_DESCRIPTION'
MAIN_QUERY_BEGIN                 = 'MAIN_QUERY_BEGIN'
MAIN_QUERY                       = 'MAIN_QUERY'
POST_DESCRIPTION_BEGIN           = 'POST_DESCRIPTION_BEGIN'
POST_DESCRIPTION                 = 'POST_DESCRIPTION'
POST_QUERY_BEGIN                 = 'POST_QUERY_BEGIN'
POST_QUERY                       = 'POST_QUERY'

#  TEST_BEGIN
#     +- ANY EMPTY  ->  TEST_BEGIN
#     +- CMT TEXT   ->  TEST_NAME !
#  TEST_NAME
#     +- CMT TEXT   ->  TEST_DESCRIPTION
#  TEST_DESCRIPTION
#     +- CMT TEXT   ->  TEST_DESCRIPTION
#     +- CMT EMPTY  ->  TEST_DESCRIPTION
#     +- CMT PARAM  ->  TEST_DESCRIPTION
#     +- NON EMPTY  ->  EXERCISE_DESCRIPTION_BEGIN !
#  EXERCISE_DESCRIPTION_BEGIN
#     +- ANY EMPTY  ->  EXERCISE_DESCRIPTION_BEGIN
#     +- CMT ANY    ->  MAIN_DESCRIPTION !
#  MAIN_DESCRIPTION
#     +- CMT TEXT   ->  MAIN_DESCRIPTION
#     +- CMT EMPTY  ->  MAIN_DESCRIPTION
#     +- CMT PARAM  ->  MAIN_DESCRIPTION
#     +- NON ANY    ->  MAIN_QUERY !
#  MAIN_QUERY
#     +- NON TEXT   ->  MAIN_QUERY 
#     +- CMT ANY    ->  POST_DESCRIPTION_BEGIN !
#  POST_DESCRIPTION_BEGIN
#     +- CMT EMPTY  ->  POST_DESCRIPTION_BEGIN 
#     +- CMT TAG    ->  POST_DESCRIPTION 
#     +- CMT ANY    ->  EXERCISE_DESCRIPTION_BEGIN !
#  POST_DESCRIPTION
#     +- CMT TEXT   ->  POST_DESCRIPTION
#     +- CMT EMPTY  ->  POST_DESCRIPTION
#     +- CMT PARAM  ->  POST_DESCRIPTION
#     +- NON ANY    ->  POST_QUERY !
#  POST_QUERY
#     +- NON ANY    ->  POST_QUERY 
#     +- CMT ANY    ->  POST_DESCRIPTION_BEGIN !

# Token
#
#    token = {
#        'is_comment' : bool,
#        'is_empty'   : bool,
#        'is_text'    : bool,
#        'is_param'   : bool,
#        'is_tag'     : bool,
#        'text'       : str,
#        'key'        : str,
#        'value'      : str,
#        'tag'        : str,
#        'line'       : str,
#        'line_no'    : int
#    }




class Parser:
    def __init__(self):
        self.test_file = None

        self.state = TEST_BEGIN
        self.token = None

        self.test_name = None
        self.test_description_line_no = None
        self.test_description = None
        self.test_params = {}

        self.exercises = {}

        self.exercise_no = 0
        self.exercise_params = {}
        self.main_description_line_no = None
        self.main_description = None
        self.main_query_line_no = None
        self.main_query = None
        self.post_tag = None
        self.post_description_line_no = None
        self.post_description = None
        self.post_query_line_no = None
        self.post_query = None
        self.posts = {}

        self.description_paragraph_acc = []
        self.description_acc = []
        self.query_acc = []

    def is_comment(self):
        return self.token['is_comment']
    def is_empty(self):
        return self.token['is_empty']
    def is_text(self):
        return self.token['is_text']
    def is_param(self):
        return self.token['is_param']
    def is_tag(self):
        return self.token['is_tag']
    def text(self):
        return self.token['text']
    def key(self):
        return self.token['key'].strip()
    def value(self):
        return self.token['value'].strip()
    def tag(self):
        return self.token['tag']
    def line(self):
        return self.token['line']
    def line_no(self):
        return self.token['line_no']

    def set_test_name(self):
        log.debug('                     ACTION: SET TEST NAME')
        self.test_name = self.text()

    # TEST_DESCRIPTION
    def add_test_description_paragraph(self):
        log.debug('                     ACTION: ADD TEST_DESCRIPTION_PARAGRAPH')
        self.description_paragraph_acc.append(self.text())
    def end_test_description_paragraph(self):
        if not self.description_paragraph_acc:
            return
        log.debug('                     ACTION: END TEST_DESCRIPTION_PARAGRAPH')
        self.description_acc.append(
            ' '.join(self.description_paragraph_acc)
        )
        self.description_paragraph_acc = []
    def end_test_description(self):
        if self.description_paragraph_acc:
            self.end_test_description_paragraph()
        self.test_description = '\n\n'.join(self.description_acc)
        self.description_acc = []
        log.debug('                     ACTION: END TEST_DESCRIPTION',self.test_description)
    def set_test_param(self):
        log.debug('                     ACTION: SET TEST_PARAM')
        if self.key() in self.test_params:
            log.error('Attempting to set the same test parameter %s twice. Second occurrence on line %d.'%(self.key(),self.line_no()))
        self.test_params[self.key()] = self.value()


    # EXERCISE 
    def set_exercise_param(self):
        log.debug('                     ACTION: SET EXERCISE_PARAM')
        if self.key() in self.exercise_params:
            log.error('Attempting to set the same exercise parameter %s twice. Second occurrence on line %d.'%(self.key(),self.line_no()))
        self.exercise_params[self.key()] = self.value()
    
    # MAIN
    def add_main_description_paragraph(self):
        if not self.main_description_line_no:
            self.main_description_line_no = self.line_no()
        log.debug('                     ACTION: ADD MAIN_DESCRIPTION_PARAGRAPH')
        self.description_paragraph_acc.append(self.text())
    def end_main_description_paragraph(self):
        if not self.description_paragraph_acc:
            return
        log.debug('                     ACTION: END MAIN_DESCRIPTION_PARAGRAPH')
        self.description_acc.append(
            ' '.join(self.description_paragraph_acc)
        )
        self.description_paragraph_acc = []
    def end_main_description(self):
        if self.description_paragraph_acc:
            self.end_main_description_paragraph()
        log.debug('                     ACTION: END MAIN_DESCRIPTION')
        self.main_description = '\n\n'.join(self.description_acc)
        self.description_acc = []
    def add_main_query(self):
        log.debug('                     ACTION: ADD MAIN QUERY')
        if not self.main_query_line_no:
            self.main_query_line_no = self.line_no()
        self.query_acc.append(self.text())
    def end_main_query(self):
        log.debug('                     ACTION: END MAIN QUERY')
        self.main_query = '\n'.join(self.query_acc)
        self.query_acc = []

    def debug_post(self):
        log.debug('                       post_tag', self.post_tag)
        log.debug('                       query_acc', self.query_acc)
        log.debug('                       parag_acc', self.description_paragraph_acc)
        log.debug('                       descr_acc', self.description_acc)

    # POST
    def set_post_tag(self):
        self.post_tag = self.tag()
        log.debug('                     ACTION: SET POST_TAG')
        self.debug_post()
    def add_post_description_paragraph(self):
        if not self.post_description_line_no:
            self.post_description_line_no = self.line_no()
        log.debug('                     ACTION: ADD POST_DESCRIPTION_PARAGRAPH')
        self.description_paragraph_acc.append(self.text())
        self.debug_post()
    def end_post_description_paragraph(self):
        if not self.description_paragraph_acc:
            return
        log.debug('                     ACTION: END POST_DESCRIPTION_PARAGRAPH')
        self.description_acc.append(
            ' '.join(self.description_paragraph_acc)
        )
        self.description_paragraph_acc = []
        self.debug_post()
    def end_post_description(self):
        if self.description_paragraph_acc:
            self.end_post_description_paragraph()
        log.debug('                     ACTION: END POST_DESCRIPTION')
        self.post_description = '\n\n'.join(self.description_acc)
        self.description_acc = []
        self.debug_post()
    def add_post_query(self):
        log.debug('                     ACTION: ADD POST QUERY')
        if not self.post_query_line_no:
            self.post_query_line_no = self.line_no()
        self.query_acc.append(self.text())
        self.debug_post()
    def end_post_query(self):
        log.debug('                     ACTION: END POST QUERY')
        self.post_query = '\n'.join(self.query_acc)
        self.query_acc = []
        self.debug_post()
    def end_post(self):
        if self.query_acc:
            self.end_post_query()
        log.debug('                     ACTION: END POST')
        if self.post_tag:
            self.posts[self.post_tag] = {
                'description'         : self.post_description,
                'description_line_no' : self.post_description_line_no,
                'query'               : self.post_query,
                'query_line_no'       : self.post_query_line_no,
            }

        self.post_tag = None
        self.post_description = None
        self.post_description_line_no = None
        self.post_query = None
        self.post_query_line_no = None
        self.debug_post()

    def end_exercise(self):
        if not self.main_description:
            return 
        if self.post_tag:
            self.end_post()
        if self.query_acc:
            self.end_main_query()

        log.debug('                     ACTION: END EXERCISE')
        self.exercise_no += 1
        exercise = {
            'exercise_no'              : self.exercise_no,
            'params'                   : self.exercise_params,
            'main_description'         : self.main_description,
            'main_description_line_no' : self.main_description_line_no,
            'main_query'               : self.main_query,
            'main_query_line_no'       : self.main_query_line_no,
            'posts'                    : self.posts
        }
        self.exercises[self.exercise_no] = exercise

        log.debug('EXERCISE:', utils.to_pretty_str(exercise))
        self.exercise_params = {}
        self.main_description_line_no = None
        self.main_description = None
        self.main_query_line_no = None
        self.main_query = None
        self.post_tag = None
        self.post_description_line_no = None
        self.post_description = None
        self.post_query_line_no = None
        self.post_query = None
        self.posts = {}

        self.description_paragraph_acc = []
        self.description_acc = []
        self.query_acc = []

    
        

    # # returns True if the token has been consumed
    # # otherwise a state transition has occurred 
    # # but the token needs to be resubmitted for consumption
    def transition(self):
        #print(utils.to_pretty_str(self))
        log.debug('STATE: %-25s            LINE %-2d: %s'%(self.state, self.line_no(),self.line()))
        if self.state == TEST_BEGIN:
            if self.is_empty():
                log.debug('                     ACTION: IGNORE')
                return True
            elif self.is_comment() and self.is_text():
                log.debug('                     ACTION: CHANGE STATE AND REPEAT')
                self.state = TEST_NAME
                return False

        elif self.state == TEST_NAME:
            if self.is_comment():
                self.set_test_name()
                self.state = TEST_DESCRIPTION
                return True

        elif self.state == TEST_DESCRIPTION:
            if self.is_comment():
                if self.is_text():
                    self.add_test_description_paragraph()
                    return True
                elif self.is_empty():
                    self.end_test_description_paragraph()
                    return True
                elif self.is_param():
                    self.set_test_param()
                    return True
            elif self.is_empty(): # and not self.is_comment()
                self.end_test_description()
                log.debug('                     ACTION: CHANGE STATE AND REPEAT')
                self.state = EXERCISE_DESCRIPTION_BEGIN
                return False

        elif self.state == EXERCISE_DESCRIPTION_BEGIN:
            if self.is_empty():
                log.debug('                     ACTION: IGNORE')
                return True
            elif self.is_comment():
                log.debug('                     ACTION: CHANGE STATE AND REPEAT')
                self.state = MAIN_DESCRIPTION
                return False
                
        elif self.state == MAIN_DESCRIPTION:
            if self.is_comment():
                if self.is_text():
                    self.add_main_description_paragraph()
                    return True
                elif self.is_empty():
                    self.end_main_description_paragraph()
                    return True
                elif self.is_param():
                    self.set_exercise_param()
                    return True
            else: # not self.is_comment():
                self.end_main_description()
                log.debug('                     ACTION: CHANGE STATE AND REPEAT')
                self.state = MAIN_QUERY
                return False

        elif self.state == MAIN_QUERY:
            if not self.is_comment():
                if self.is_text():
                    self.add_main_query()
                return True
            if self.is_comment():
                self.end_main_query()
                log.debug('                     ACTION: CHANGE STATE AND REPEAT')
                self.state = POST_DESCRIPTION_BEGIN
                return False

        elif self.state == POST_DESCRIPTION_BEGIN:
            if self.is_comment() and self.is_empty():
                log.debug('                     ACTION: IGNORE')
                return True
            elif self.is_comment() and self.is_tag():
                self.set_post_tag()
                log.debug('                     ACTION: CHANGE STATE')
                self.state = POST_DESCRIPTION 
                return True
            elif self.is_comment() and self.is_text():
                self.end_exercise()
                log.debug('                     ACTION: CHANGE STATE AND REPEAT')
                self.state = EXERCISE_DESCRIPTION_BEGIN
                return False

        elif self.state == POST_DESCRIPTION:
            if self.is_comment():
                if self.is_text():
                    self.add_post_description_paragraph()
                    return True
                elif self.is_empty():
                    self.end_post_description_paragraph()
                    return True
                elif self.is_param():
                    self.set_exercise_param()
                    return True
            else: # not self.is_comment():
                self.end_post_description()
                log.debug('                     ACTION: CHANGE STATE AND REPEAT')
                self.state = POST_QUERY 
                return False 

                
        elif self.state == POST_QUERY:
            if not self.is_comment():
                if self.is_text():
                    self.add_post_query()
                return True
            if self.is_comment() :
                self.end_post()
                log.debug('                     ACTION: CHANGE STATE AND REPEAT')
                self.state = POST_DESCRIPTION_BEGIN
                return False

        self.unexpected_token()

    def parse(self, test_file):
        self.test_file = test_file
        contents = open(self.test_file,'r').read()
        #print(contents)

        for line_no, line in enumerate(contents.split('\n')):
            
            self.token = tokenize(line_no, line)
            while not self.transition():
                pass
        else:
            self.end_exercise()

        return {
            'test_name'                : self.test_name,
            'test_description'         : self.test_description,
            'test_description_line_no' : self.test_description_line_no,
            'params'                   : self.test_params,
            'exercises'                : self.exercises,
        }


    def unexpected_token(self):
        if self.state == TEST_BEGIN:
            log.error('At line', self.line_no(), 'of %s: expected comment with the test name but got \n\n%s\n\nThere should not be any SQL code, parameter declaration $key:value, or a tag #tag_name'%(self.test_file,self.line()))

        if self.state == TEST_DESCRIPTION:
            if not self.is_comment() and not self.is_empty():
                log.error('At line', self.line_no(), 'of %s: expected an empty line that separates the header from the first exercise description but got\n\n%s\n\nMake sure that the header with test name and test description is well separated from the exercises with at least one empty line'%(self.test_file,self.line()))

        if self.state == EXERCISE_DESCRIPTION_BEGIN:
            if not self.is_comment() and not self.is_empty():
                log.error('At line', self.line_no(), 'of %s: expected a begining of an exercise description but got non-comment non-empty line\n\n%s'%(self.test_file,self.line()))

        if self.state == MAIN_DESCRIPTION:
            if self.is_comment() and self.is_tag():
                log.error('At line', self.line_no(), 'of %s: unexpected tag line\n\n%s\n\nMake sure not to use any tags in the main description of an exercise'%(self.test_file,self.line()))

        if self.state == POST_DESCRIPTION:
            if self.is_comment() and self.is_tag():
                log.error('At line', self.line_no(), 'of %s: unexpected tag line\n\n%s\n\nMake sure to use tags only at the beginning of additional parts of an exercise'%(self.test_file,self.line()))

        log.error('While in state',self.state,'encountered unexpected token', self.token)
    


#
# test : {
#    'test_name'                : str,
#    'test_description'         : str,
#    'test_description_line_no' : int,
#    'params'                   : {key:value},
#    'exercises'                : {
#         'exercise_no'                : int,
#         'params'                     : {key:value},
#         'main_description'           : str,
#         'main_description_line_no'   : int,
#         'verification_description'   : str,
#         'verification_description_line_no' : int,
#         'main_query'                 : str,
#         'main_query_line_no'         : int,
#         'verification_query'         : str,
#         'verification_query_line_no' : int,
#    }
#


def parse(test_file):
    return check_test_integrity(Parser().parse(test_file))
    

SUPPORTED_TEST_PARAMS = [
    'main_db',
    'grading_dbs',
    'exclude_main_db_from_grading',
    'max_relative_grade', 
    'grading_aggregate',
    'penalty_for_wrong_attribute_names',
    'penalty_for_results_not_distinct',
    'penalty_for_wrong_order'
]

SUPPORTED_GRADING_AGGREGATES = [
    'min'
]

SUPPORTED_EXERCISE_PARAMS = [
    'show_verification_query',
    'show_display_query',
    'show_results',
    'show_results_row_count',
    'penalty_for_wrong_attribute_names',
    'penalty_for_results_not_distinct',
    'penalty_for_wrong_order',
    'show_update_row_count',
    'points',
    'mode'
]

SUPPORTED_MODES = ['read_only', 'rollback']


def check_test_integrity(t):
    for key in t['params']:
        if key not in SUPPORTED_TEST_PARAMS:
            log.error('Unrecognized test parameter %s'%(key,))
    if 'grading_aggregate' in t['params'] and t['params']['grading_aggregate'] not in SUPPORTED_GRADING_AGGREGATES:
        log.error('Unsupported grading aggregate %s'%(t['grading_aggregate']))
    for ex_no in t['exercises']:
        ex = t['exercises'][ex_no]
        for key in ex['params']:
            if key not in SUPPORTED_EXERCISE_PARAMS:
                log.error('In exercise %d: Unrecognized exercise parameter %s'%(ex_no,key,))
        if 'mode' in ex['params'] and ex['params']['mode'] not in SUPPORTED_MODES:
            log.error('In exercise %d: Unrecognized exercise execution mode %s'%(ex_no,ex['mode'],))

    return t

if __name__ == '__main__':
    with open('../test/parser-test.txt','w') as f:
        f.write("""



        -- Test name.
        --
        -- This is 
        -- test description
        -- $max_relative_grade: 20
        --
        -- It comprises of 
        -- many paragraphs
        -- $main_db: test.db

        -- Info text 1.1
        -- Info text 1.2
        -- 
        -- Info text 1.3
        SELECT * FROM R;
        -- Info 2.1
        --
        -- Info 
        UPDATE R 
        WHERE B = 1
        SET A = 0;

        -- #display_query
        SELECT * FROM R WHERE B = 1;

        -- #verification_query
        SELECT DISTINCT A FROM R;
        
        -- Info 3
        -- $points: 1
        -- $mode: read_only
        DELETE FROM R WHERE A IS NULL;

        -- #verification_query
        -- What is that
        -- $show_verification_query: true
        SELECT * FROM R;
        
        -- #display_query

        SELECT * FROM R WHERE B = 1;

        """)
        f.close()

    test_spec = parse('../test/parser-test.txt')
    log.info(utils.to_pretty_str(test_spec))


