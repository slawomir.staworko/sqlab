import assess

comment=['--']
order_keywords=['ORDER BY']
unique_keywords=['DISTINCT']
transaction_keywords=['COMMIT','ROLLBACK','SET TRANSACTION']
write_keywords=['INSERT','UPDATE','DELETE','ALTER','CREATE','DROP']
update_keywords=['INSERT','UPDATE','DELETE']

def is_comment(line):
    return any([line.startswith(c) for c in comment])

def remove_comment_prefix(line):
    for c in comment:
        if line.startswith(c):
            return line[len(c):]

def is_order_query(query):
    return any([c.upper() in query.upper() for c in order_keywords])

def is_unique_query(query):
    return any([c.upper() in query.upper() for c in unique_keywords])

def is_read_only(query):
    return not any([c.upper() in query.upper() for c in write_keywords])

def is_update(query):
    return any([c.upper() in query.upper() for c in update_keywords])

def is_transactional(query):
    return any([c.upper() in query.upper() for c in transaction_keywords])

def guess_penalty_metric(query):
    metric = {}

    if not is_order_query(query):
        metric[assess.RESULT_ORDER] = '0%'

    if not is_unique_query(query):
        metric[assess.RESULT_UNIQUE] = '0%'
        
    return metric

def guess_query_mode(query):
    if is_read_only(query):
        return 'read_only'
    else:
        return 'rollback'


