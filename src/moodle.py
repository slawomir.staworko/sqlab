import os
import xml.sax.saxutils

class MoodleWriter:
    def __init__(self, env, articulator):
        self.env = env
        self.articulator = articulator
        self.moodle_file_name = os.path.join(
            #self.env.config['PREPARED_DIR'],
            self.env.config['MOODLE_XML']
        )

    def generate_moodle_file(self):
        self.moodle_file = open(self.moodle_file_name,'w')
        self.handler = xml.sax.saxutils.XMLGenerator(
            self.moodle_file,
            encoding='utf-8',
            short_empty_elements=True
        )
        self.handler.startDocument()
        self.handler.startElement("quiz",{})
        for ex_no in self.env.test['exercises']:
            self.handler.startElement("question",{"type": "essay",})
            
            self.handler.startElement("name",{})
            self.handler.startElement("text",{})
            self.handler.characters("%02d"%ex_no)
            self.handler.endElement("text")
            self.handler.endElement("name")

            self.handler.startElement("questiontext",{"format": "html",})
            self.handler.startElement("text",{})
            self.handler.characters(
                self.articulator.get_exercise_description_html(ex_no)
            )
            self.handler.endElement("text")
            self.handler.endElement("questiontext")

            self.handler.startElement("generalfeedback",{"format": "html",})
            self.handler.startElement("text",{})
            self.handler.endElement("text")
            self.handler.endElement("generalfeedback")

            self.handler.startElement("defaultgrade",{})
            self.handler.characters("""1.0000000""")
            self.handler.endElement("defaultgrade")

            self.handler.startElement("penalty",{})
            self.handler.characters("""0.0000000""")
            self.handler.endElement("penalty")
            self.handler.startElement("hidden",{})
            self.handler.characters("""0""")
            self.handler.endElement("hidden")

            self.handler.startElement("responseformat",{})
            self.handler.characters("""editor""")
            self.handler.endElement("responseformat")

            self.handler.startElement("responserequired",{})
            self.handler.characters("""0""")
            self.handler.endElement("responserequired")

            self.handler.startElement("responsefieldlines",{})
            self.handler.characters("""15""")
            self.handler.endElement("responsefieldlines")

            self.handler.startElement("attachments",{})
            self.handler.characters("""0""")
            self.handler.endElement("attachments")

            self.handler.startElement("attachmentsrequired",{})
            self.handler.characters("""0""")
            self.handler.endElement("attachmentsrequired")

            self.handler.startElement("graderinfo",{"format": "html",})
            self.handler.startElement("text",{})
            self.handler.endElement("text")
            self.handler.endElement("graderinfo")

            self.handler.startElement("responsetemplate",{"format": "html",})
            self.handler.startElement("text",{})
            self.handler.endElement("text")
            self.handler.endElement("responsetemplate")

            self.handler.endElement("question")

        self.handler.endElement("quiz")
        self.handler.endDocument()
        self.moodle_file.close()


if __name__ == '__main__':
    with open('../test/moodle-queries.sql','w') as f:
        f.write("""
        -- Timeout test.
        -- $main_db:../test/uni.db

        -- Fast Query
        -- 
        -- And another paragraph.
        -- $points: 10
        SELECT ID,First_Name,Height FROM Student;

        -- Fast Query
        -- 
        -- And another paragraph.
        -- $points: 10
        UPDATE Student SET Height = Height/100.0
        -- #POST_QUERY
        -- Bam, bam
        SELECT ID,First_Name,Height FROM Student;
        """)
        f.close()

    import env,prepare
    env = env.create_env('prepare ../test/moodle-queries.sql',['test_file'])
    print(env)

    if not os.path.exists(env.config['PREPARED_DIR']):
        os.mkdir(env.config['PREPARED_DIR'])

    qa = prepare.QueryArticulator(env)
    mw = MoodleWriter(env,qa)
    
    mw.generate_moodle_file()
