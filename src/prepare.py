import os
import html
import shutil
import string
import pypandoc
from lxml import etree

import log
import moodle
import template

class FormatConverter:
    def __init__(self, env):
        self.env = env
        xslt_path = os.path.join(
            self.env.config['TEMPLATES_DIR'],
            self.env.config['HTML_STYLEUP_XSLT']
        )

        self.html_styleup_transform = etree.XSLT(
            etree.XML(open(xslt_path,'r').read())
        )


    def to_html(self, md):
        html = pypandoc.convert_text(md,'html5', 
                                     format='md')
        
        html_tree = etree.XML('<root>'+html+'</root>')
    
        result_tree = self.html_styleup_transform(html_tree)

        return str(result_tree)


    def to_standalone_tex(self, md):
        return pypandoc.convert_text(md,'latex', 
                                     format='md',
                                     extra_args=['--standalone'])

    def to_standalone_html(self, md):
        return pypandoc.convert_text(md,'html5', 
                                     format='md',
                                     extra_args=['--standalone'])
        
    def to_text(self, md):
        return pypandoc.convert_text(md,'plain', 
                                     format='md',
                                     extra_args=['--columns=78'])




class MarkdownRenderer:
    def __init__(self, env):
        self.env = env

    def format_table_bar(self, widths,hbar='-',sep='+'):
        return sep + hbar + (hbar+sep+hbar).join([hbar*w for w in widths]) + hbar + sep

    def format_table_cell(self,width,text):
        if text is None:
            text = 'NULL'
        text = str(text)
        if len(text) <= width:
            return text + ' '*(width-len(text)) 
        else:
            return text[:width-3] + '...'

    def format_table_line(self,widths,line,sep='|'):
        return (sep + ' ' + 
                (' '+sep+' ').join([self.format_table_cell(w,c) for w,c in zip(widths,line)]) + 
                ' ' + sep)

    # query_result : {
    #     'query'   : string
    #     'columns' : tuple(str)
    #     'tuples'  : list(tuple)
    #     'error'   : string
    # }
    def render_table(self, result, show_rows):
        if result['error']:
            return result['error']
        if not result['columns']:
            return ''

        if show_rows == 'all':
            SHOW_ROWS = len(result['tuples'])
        else:
            SHOW_ROWS = int(show_rows)
                
        MIN_COLUMN_WIDTH = int(self.env.config['MIN_COLUMN_WIDTH'])
        MAX_COLUMN_WIDTH = int(self.env.config['MAX_COLUMN_WIDTH'])

        columns = result['columns']
        row_count = min(len(result['tuples']),SHOW_ROWS)
        head = result['tuples'][:row_count]

        column_widths = [] 
        for i, column_name in enumerate(columns):
            widths = {MIN_COLUMN_WIDTH} 
            widths.add(len(column_name))
            for t in head:
                widths.add(min(len(str(t[i])),MAX_COLUMN_WIDTH))
            column_widths.append(max(widths))

        acc = []
        acc.append(self.format_table_bar(column_widths))
        acc.append(self.format_table_line(column_widths,columns))
        acc.append(self.format_table_bar(column_widths,'='))
        for line in head:
            acc.append(self.format_table_line(column_widths,line))
            acc.append(self.format_table_bar(column_widths))
        if row_count < len(result['tuples']):
            acc.append(self.format_table_line(column_widths,['...']*len(columns)))
            acc.append(self.format_table_bar(column_widths))
        if row_count == 0:
            acc.append(self.format_table_line(column_widths,[' ']*len(columns)))
            acc.append(self.format_table_bar(column_widths))

        return '\n'.join(acc)


class QueryArticulator:
    def __init__(self,env):
        self.env = env
        self.engine = self.env.get_main_engine()
        self.template_runner = template.TemplateRunner(env)
        self.md_renderer = MarkdownRenderer(env)
        self.format_converter = FormatConverter(env)
        self.exercise_description_md = {}

        self.moodle_writer = moodle.MoodleWriter(self.env,self)

    def get_test_description_md(self):
        log.info('Generating Test Description')
        return self.template_runner.render('TEST_DESCRIPTION',self.env.test)

    def get_exercise_description_md(self,ex_no):
        if ex_no not in self.exercise_description_md:
            log.info('Generating Description for Exercise', ex_no)
            results = self.engine.display_exercise_results(ex_no)
            params = self.env.test['exercises'][ex_no].copy()
            show_rows = params['show_rows']
            params['results'] = self.md_renderer.render_table(results,show_rows)
            params['results_row_count'] = len(results['tuples'])
            if 'update_row_count' in results:
                params['update_row_count'] = results['update_row_count']

            ex_desc = self.template_runner.render('EXERCISE_DESCRIPTION',params)
            self.exercise_description_md[ex_no] = ex_desc
        return self.exercise_description_md[ex_no]
        
    def get_exercise_description_html(self,ex_no):
        return self.format_converter.to_html(self.get_exercise_description_md(ex_no))

    def prepare_files(self):
        if not os.path.exists(self.env.config['PREPARED_DIR']):
            os.mkdir(self.env.config['PREPARED_DIR'])

        log.info('Outputting test files in directory \'%s\''%(self.env.config['PREPARED_DIR'],))

        self.moodle_writer.generate_moodle_file()

        md = self.get_test_description_md() + '\n\n'
        for ex_no in self.env.test['exercises']:
            md += self.get_exercise_description_md(ex_no) + '\n\n'
        
        md_file = open(os.path.join(
            self.env.config['PREPARED_DIR'],
            self.env.config['DESCRIPTION_MD']
        ),'w')
        text_file = open(os.path.join(
            self.env.config['PREPARED_DIR'],
            self.env.config['DESCRIPTION_TXT']
        ),'w')
        html_file = open(os.path.join(
            self.env.config['PREPARED_DIR'],
            self.env.config['DESCRIPTION_HTML']
        ),'w')
        tex_file = open(os.path.join(
            self.env.config['PREPARED_DIR'],
            self.env.config['DESCRIPTION_TEX']
        ),'w')

        md_file.write(md)
        text_file.write(self.format_converter.to_text(md))
        html_file.write(self.format_converter.to_standalone_html(md))
        tex_file.write(self.format_converter.to_standalone_tex(md))

        md_file.close()
        text_file.close()
        html_file.close()
        tex_file.close()
            
def prepare_test(env):
    QueryArticulator(env).prepare_files()

if __name__ == '__main__':
    with open('../test/rendering-queries.sql','w') as f:
        f.write("""
        -- Rendering test.
        -- Blah blah
        -- 
        -- Blah Blah
        -- $main_db: ../test/uni.db


        -- Large results
        -- $show_results: all
        SELECT * FROM Enrollement;

        -- Fast Query
        -- 
        -- And another paragraph.
        -- $points: 10
        -- $show_results: 2
        SELECT * FROM Student WHERE First_Name IN ('Paul', 'Jean');

        -- Fast Query
        -- 
        -- And another paragraph.
        -- $points: 10
        UPDATE Student SET Height = Height/100.0
        -- #display_query
        -- Bam, bam
        SELECT ID,First_Name,Height FROM Student;

        -- #verification_query
        -- The following query is used for verification of correctness of your results
        -- $show_verification_query: true
        SELECT * FROM Student;
        """)
        f.close()

    import env
    env = env.create_env('prepare ../test/rendering-queries.sql',['test_file'])

    print(env)

    prepare_test(env)

    if os.path.exists('test.json'):
        os.remove('test.json')


